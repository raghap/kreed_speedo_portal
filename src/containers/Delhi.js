import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import ClubSelect from '../components/fields/ClubSelect.js';
import CheckBox from '../components/fields/CheckBox.js';
import Radio from '../components/fields/Radio.js'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import FileUploader from 'react-firebase-file-uploader';
import Indicator from '../components/Indicator'
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

// for file uploader
const uuidv1 = require('uuid/v1');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name:"Speedo-BGN Invitational Swimming Championship 2018, New Delhi",
      Kreed_ID:"",
      full_name: "",
      second_name: "",
      date_of_birth: "",
      gender: "",
      mobile_number: "",
      email: "",
      parent_name: "",
      parent_mobile_number: "",
      city: "",
      club_name: "",
      clubList: ["A One Swimming Centre","Achievers Swimming Academy","Adarsh Public School",
                "Ahlcon Public School","Aim Sports Club","Air Force Bal Bharati School",
                "Air Force Golden Jubilee Institute","Alisha Swimming Coaching Institute",
                "Amity International School","Apeejay School, Gurudwara Road","Apeejay School, Panchsheel Park",
                "Apeejay School, Sainik Vihar","Apex Public School","Army Public School",
                "Arwachin Bharti Bhawan Sr Sec School","Bal Bharati Pub School, Sector 14, Rohini",
                "Banasthali Public School","BGN Academy","BGN Sports Club","Bhartiya Vidya Bhavan'S",
                "Bhatnagar International School","Birla Vidya Niketan, Pushp Vihar Sector Iv","Birla Vidya Niketan School",
                "Bluebell'S School International","Bosco Public School","Butler Memorial Girls School",
                "C L Bhaliadayanand Model School","C R P F Public School","Cambridge Foundation School",
                "Cambridge School","Carmel Convent School","Central Academy","Chinmaya Vidyalaya Sec School",
                "Convent Of Jesus & Mary School","D.A.V.Public School, Pitam Pura","D.A.V.Public School, Vasant Kunj",
                "D.T.E.A. Sr. Sec. School, Janakpuri","D.T.E.A. Sr. Sec. School, Laxmibai Nagar",
                "D.T.E.A. Sr. Sec. School, Lodhi Estate","D.T.E.A. Sr. Sec. School, Mandir Marg","D.T.E.A. Sr. Sec. School, Moti Bag-Ii",
                "D.T.E.A. Sr. Sec. School, Pusa Road","D.T.E.A. Sr. Sec. School, R.K. Puram","Deep Pblic School",
                "Delhi Dehat Swimming Club","Delhi Jain Public School","Delhi Polic Public School","Delhi Public School, Mathura Road",
                "Delhi Public School, Sector C Pocket V","Delhi Public School, Sector Xii R. K. Puram","Delhi United Christian School",
                "Don Bosco School","Doon Public School","Dwarka Sports Academy","East Delhi Swimming Club",
                "Evergreen Public School","G D Goenka Public School","Goswami Swimming Academy",
                "Guru Harkrishan Public School, 1- Purana Quila Road","Guru Harkrishsn Public School, Vasant Vihar",
                "Gyan Bharti School","Hunter Sports Swimming Club","ITL Public School","Kendriya Vidyalaya, Sector 3 Rohini",
                "Kendriya Vidyalaya, Sector 8 Rohini","Khajan Singh Swimming Academy","Krishna Sports Club",
                "Laxmi Public School","Loreto Convent School","Mann Khurd Village","Modern School , Barakhamba Road",
                "Modern School, Vasant Vihar Poorvi Marg","Montfort School","Mother Teresa Public School",
                "Mount Abu Public School","Mount Carmel School, Anand Neketan","Mount Carmel School, Sec - 22, Phase 1, Dwarka",
                "N D M C Navayug School","Navy Children School","Navy Legends Swimming Academy","Navyug School",
                "New Delhi Public School","New Era Public School","Punjabi Bagh Club","Queen Mary'S School","Ramjas School",
                "Remal Public School","Rohini Sports Academy","Rukmini Devi Public School","Ryan International School",
                "S S Mota Singh Sr Sec Model School","Sachdeva Public School, Fp Block Maurya Enclave",
                "Sachedeva Public School, Sector 13 Rohini","Salwan Public School, RAJINDER NAGAR","Salwan Public School, KONDLI GHAROI COMPLEX",
                "Sardar Patel Vidyalaya","Shahapurt Jat Swimming Club","Somerville School","South Delhi Swim Club",
                "South Delhi Swimming Club","Springdales School, Benito Juarez Marg","Springdales School, Pusa Road",
                "St Columba'S School","St George'S School","St Mary'S School","St Xavier'S School",
                "St. Anthony'S Girls Sec. School","Sun Moon Shine Club","Tagore International School",
                "Tehlan Swim Sports Club","The Air Force School","The Army Public Schol","The Heritage School",
                "Unique Sports Club","Vasant Valley School","YMCA Academy","Young Swimming Club","Others"],
      other_club_name:"",
      swimGroup: "",
      athleteInfo: "",
      amount_to_pay: "0",
      events:"",
      eventList: [],
      selectedEvents:[],
      selectedEventTimings:[],
      // relay events
      relay_event:"",
      relayEventList: [],
      relay_team: "",
      selectedRelayEvent: [],
            
      agree_TC: "",
      selectedClubValue:"",
      isLoading: false,
      errorAlert: false,
      // athleteLoadingError: false,
      athleteObj:{},
      newKreedID:"",
      enrollmentID:"",
      // Navigation for different pages
      register: "Yes",
      thankyou: "No",
      paymentSuccess: "No",
      // Documents
      docURL: "",
      isUploading: false,
      dlProgress: new Map()
    }

    var strMeetName = this.state.meet_name.replace(/[^A-Z0-9]+/ig, "_");
    this.state.docRef = firebase.app.storage().refFromURL('gs://kreed-of-sports-speedo/' + strMeetName + "/" + uuidv1());

    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.getGroup = this.getGroup.bind(this);
    this.getEventList = this.getEventList.bind(this);
    this.getRelayEventList = this.getRelayEventList.bind(this);
    this.resetdata = this.resetdata.bind(this);
    this.submitRegistration = this.submitRegistration.bind(this);
    this.capturePayment = this.capturePayment.bind(this);
    this.getInputFields = this.getInputFields.bind(this);
    this.displayRegisteredEvents = this.displayRegisteredEvents.bind(this);
    this.payOnline = this.payOnline.bind(this);
   //for displaying docs list
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);
  }

  // for diplaying file list
  handleUploadStart(fname, task) {
    this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
    if (!this.state.isUploading)
      this.setState({ isUploading: true, progress: 0 });
  }
  handleProgress(progress, task) {
    if (progress <= 100) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
      this.setState({ avatarURL: task.snapshot.ref.fullPath });
    }
  }
  handleUploadError(error) {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess(filename, task) {
    // this.setState({avatar: filename, progress: 100, isUploading: false})
    this.state.dlProgress.delete(task.snapshot.ref.fullPath);
    if (this.state.dlProgress.size == 0)
      this.state.isUploading = false;
      this.state.docRef.child(filename).getDownloadURL()
      .then(url => {
        this.setState({ docURL: url });
      })
      .catch(error => {
        alert("err "+error)
      })
  };

   //displaying file names
   displayList() {
    var i;
    var items = [];

    var link = this.state.docURL;
    var pts = link.split('/');
    var fn = unescape(pts[pts.length - 1].split('?')[0]);
    var fnparts = fn.split('/');
    items.push(fnparts[2]);
    
    return items;
  }

  resetdata() {
    var newState = {};
      newState.full_name = "";
      newState.second_name = "";
      newState.date_of_birth = "",
      newState.gender = "",
      newState.mobile_number = "",
      newState.email = "",
      newState.parent_name = "",
      newState.parent_mobile_number = "",
      newState.events = "",
      newState.city = "",
      newState.club_name = "",
      newState.selectedEvents = [],
      newState.selectedEventTimings = [],
      newState.ksa_id = "",
      newState.eventList = [],
      newState.selectedClubValue = ""
    this.setState(newState);

  }
  getEventList() {
    var events = [];

    this.state.amount_to_pay = "600";

    if (this.state.swimGroup == "Masters Group A" || this.state.swimGroup == "Masters Group B" || 
          this.state.swimGroup == "Masters Group C" || this.state.swimGroup == "Seniors" || 
          this.state.swimGroup == "Group VI") 
    {
      events = ["50m Freestyle", "50m Breast Stroke"];
    } 
    else if (this.state.swimGroup == "Group I" || this.state.swimGroup == "Group II" || 
             this.state.swimGroup == "Group III" || this.state.swimGroup == "Group IV" || 
             this.state.swimGroup == "Group V") 
    {
      events = ["50m Freestyle", "50m Breast Stroke","50m Back Stroke","50m Butterfly"];
    } 

    this.setState({ eventList: events })
  }
  getRelayEventList(){
    var relayEvents = [];
    if (this.state.swimGroup == "Group I" || this.state.swimGroup == "Group II" || 
             this.state.swimGroup == "Group III" || this.state.swimGroup == "Group IV" || 
             this.state.swimGroup == "Group V") 
    {
      relayEvents = ["4x50m Mixed Freestyle Relay"];
    } 
    this.setState({ relayEventList: relayEvents })
  }
  // to get group
  getGroup(dob) {
    var timestamp = Date.parse(dob);

    if (isNaN(timestamp))
      return "Open";

    var current = new Date();
    var dob_dt = new Date(dob);
    var today = current.getFullYear() - dob_dt.getFullYear();

    if (today > 51) {
      return "Masters Group C";
    } else if (today == 18 || today == 19 || today == 20 || today == 21 || today == 22 || today == 23 
              || today == 24 || today == 25 || today == 26 ||today == 27 || today == 28 || today == 29) {
      return "Seniors";
    } else if (today == 17 || today == 16 || today == 15) {
      return "Group I";
    } else if (today == 14 || today == 13) {
      return "Group II";
    } else if (today == 12 || today == 11) {
      return "Group III";
    } else if (today == 10 || today == 9) {
      return "Group IV";
    } else if (today == 8 || today == 7) {
      return "Group V";
    } else if (today == 6 || today == 5) {
      return "Group VI";
    } else if (today == 30 || today == 31 || today == 32 || today == 33 || today == 34 || today == 35 
                || today == 36 || today == 37 || today == 38 || today == 39 ||today == 40 ) {
      return "Masters Group A";
    } else if (today == 41 || today == 42 || today == 43 || today == 44 || today == 45 || today == 46 ||
               today == 47 || today == 48 || today == 49 || today == 50 ||today == 51 ) {
      return "Masters Group B";
    } else {
      return "Kids below 5 years of age are not permitted."
    }
  }
  componentDidMount() {

  }
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
    if (name == "parent_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
   

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }

    if(name.startsWith("eventTiming")){
      var index = name.split("_")[1];
      var timings = this.state.selectedEventTimings;
      timings[index] = value;
      this.setState({selectedEventTimings: timings});
      return;
    }

    this.setState({
      [name]: value
    });
    // alert(this.state.events);
  }
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/assoc/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {

        if (key == "date_of_birth") {
          this.state.selectedEvents = "";
          this.state.events = "";
          this.state.relay_event = ""
          this.state.swimGroup = this.getGroup(value);
          this.getEventList();
          this.getRelayEventList();
        }

        //Checkbox handling
        if(key == "events"){
        
          if(typeof(value) == "string")
            this.state.selectedEvents = value.slice(1).split(",");
          else
          {
            this.state.selectedEvents = [];
            value = "";
          }
        }
        
        if(key == "relay_event") {
            if(typeof(value) == "string")
            {
              this.state.selectedRelayEvent = value.slice(1).split(",");   
              this.state.amount_to_pay = "850";
            }
            else
            {
              this.state.selectedRelayEvent = [];
              this.state.amount_to_pay = "600";
              value = "";
            }
    
        }

        this.setState({ [key]: value });
      }

    }
  }
  getInputFields(){
    var i;
    var items = [];
    var event_name = [];
    var selectedEvtTime = []
    for (i = 0; i < this.state.selectedEvents.length; i++) {
      items.push(
      <div className="row">
      <div className="col-sm-2">
        <h4 className="eventname-styling">
        {this.state.selectedEvents[i]}
        </h4>
      </div>
      <div className="col-sm-4">
       <input type="text" placeholder="00:00.00" name={ "eventTiming_" + i} aria-required="true" value={this.state.selectedEventTimings[i]} onChange={this.handleInputChange} className="form-control" />
      </div>
    </div> 
      );
    }
    return items;
  }
  displayRegisteredEvents(){
    var i;
    var items = [];

    for (i = 0; i <  this.state.selectedEvents.length; i++) {
      items.push(<li>{this.state.selectedEvents[i]}</li>);
    }
    for (i = 0; i <  this.state.selectedRelayEvent.length; i++) {
      items.push(<li>{this.state.selectedRelayEvent[i]}</li>);
    }
    return items;
  }



  getDataObject()
  {
      var dObj = {};

        if(this.state.city == ""){
          this.state.city = "Delhi";
        }

        dObj.full_name = this.state.full_name;
        dObj.date_of_birth = this.state.date_of_birth;
        dObj.gender = this.state.gender;
        
        dObj.parent_name = this.state.parent_name;
        dObj.swimGroup = this.state.swimGroup;
        dObj.city = this.state.city;
        dObj.country = "India";

        //common fields
        dObj.mobile_number = this.state.mobile_number;
        dObj.email = this.state.email;
        dObj.parent_mobile_number = this.state.parent_mobile_number;
        dObj.club_name = this.state.club_name;
        dObj.amount = this.state.amount_to_pay;

        //event regn fields - always carried.
        dObj.brand_name = "speedo";
        dObj.meet_name = this.state.meet_name;
        dObj.selected_events = this.state.selectedEvents.slice();
        dObj.selected_event_timings = this.state.selectedEventTimings.slice();
        dObj.selected_relay_events = this.state.selectedRelayEvent.slice();
        dObj.relay_team_members = this.state.relay_team;
        dObj.document_path = this.state.docURL;

    return dObj;

  }

  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.state.newKreedID, enrollment_id: this.state.enrollmentID, payment_id: payResp.razorpay_payment_id, brand_name: "speedo", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true ,thankyou: "No",paymentSuccess: "Yes"  });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }

  payOnline(){
    var options = {
      "key": "rzp_live_SKW6ZcvymAaWBJ",
      "amount": this.state.amount_to_pay * 100 , // 2000 paise = INR 20
      "name": "SwimIndia",
      "description": "Speedo Invitational Swimming Championship 2018, Bangalore",
      // "image": "/your_logo.png",
      "handler": this.capturePayment,
      "prefill": {
          "contact": this.state.mobile_number,
          "email": this.state.email,
      },
      "notes": {
          "address": this.state.city
      },
      "theme": {
          "color": "#528FF0"
      }
    };
    var rzp = new window.Razorpay(options);
    rzp.open();

  }
  submitRegistration(e)
  {
      e.preventDefault();
      if(this.state.isUploading){
        this.state.errorStatement = "Please wait Uploading your document";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.date_of_birth == "" ){
        this.state.errorStatement = "Select Your Date Of Birth";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.docURL == "" ){
        this.state.errorStatement = "Please upload your DOB proof";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.gender === "" ){
        this.state.errorStatement = "Please ensure you have selected a gender";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.mobile_number.length < 10 ){
        this.state.errorStatement = "Please ensure you have entered a 10 digit mobile number";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == ""){
        this.state.errorStatement = "Select Your Club";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == "Others" && this.state.other_club_name == ""){
        this.state.errorStatement = "Enter Your Club name";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length < 1 ){
        this.state.errorStatement = "Select atleast one event";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length >= 3 ){
        this.state.errorStatement = "You can select a maximum of 2 events";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == "Others"){
        this.state.club_name = this.state.other_club_name
      }

      this.setState({ isLoading: true });
      axios({
        method: 'post',
        headers:  {
                      'Content-Type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                  },
        url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitMeetRegistration',
        data: this.getDataObject()
      })
      .then(response => {
        // alert("KreedId" + response.data.kreedID + ", Enrolment Id: " + response.data.enrollmentID);
        this.state.newKreedID = response.data.kreedID;
        this.state.enrollmentID = response.data.enrollmentID;
        // Navigating to thankyou PAGE
        this.setState({register: "No" , thankyou: "Yes",isLoading: false });
      })
      .catch(error => {
        this.state.errorStatement = "Error completing registration";
        this.setState({ isLoading: false, errorAlert: true })
        // console.log("Error submitting athlete details: ", error);
      })
 }


//Container Render Method
render() {
  return (
    <div className="content">
       <img alt="registration are closed" src='assets/img/registration_closed_delhi.jpg' />
    </div>
  )
}
  render_unused(){
    return (
      
      <div className="content">
      {/* SWEET ALERTS */}
      <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
                <img className="banner-img-styling" src='assets/img/Speedo_BGN_Invitational_Swimming_Championship_2018_New_Delhi_wallpaper.jpg' />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
                <img className="wrapper-img-styling" src='assets/img/Speedo_BGN_Invitational_Swimming_Championship_2018_New_Delhi_wrapper.png' />
                <h2>{this.state.meet_name}</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Main Form */}
          {this.state.register == "Yes" && <div> <div className="row whitebg border padding-20">
            {/* Address */}
             <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <p className="margin-0px"><b>Venue: </b>Dr. Shyama Prasad Mukherjee Swimming Pool Complex (Talkatora Swimming Pool), Central Ridge Forest Area, Talkatora Road, New Delhi</p>
                <p className="margin-0px"><a target="_blank" className="linkStyling text-color" href="https://www.google.co.in/maps/place/Dr+Syama+Prasad+Mookerjee+Swimming+Pool+Complex/@28.6223119,77.1936506,15z/data=!4m5!3m4!1s0x0:0x2f9b31068bb77176!8m2!3d28.6223119!4d77.1936506?shorturl=1"><b>Location Map</b></a></p>
                <p className="margin-0px"><b>Dates: </b> 19 August, 2018</p>
                <p className="margin-0px"><b>Competition Start Time: </b>10am</p>
                Visit <a target="_blank" className="linkStyling" href="http://swimindia.in/speedo-bgn-invitational-swimming-championship-2018-delhi">SwimIndia</a> to know more about the meet.
              </div>
            </div>
            {/* Address */}
            {/* Form */}
            <form onSubmit={this.submitRegistration}>
            <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <label className="control-label label-color">Swimmer Name <span className="color-red"> *</span></label>
              </div>
              {/* First Name */}
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* First Name */}
              {/* Date of birth */}
              <div className="col-sm-12">
                <label className="control-label label-color">Date Of Birth <span className="color-red"> *</span></label>
                <div><span style={{ fontSize: 12 }}>Please select your DOB to view Groups and Events</span></div>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                     <KreedDatePicker theKey="date_of_birth" required="true" value={this.state.date_of_birth} updateAction={this.updateAction} />
                  </div>
                </div>
              </div>
              {/* Date of birth */}
                    {/* Documents */} 
                      <div className="col-sm-12">
                        <label className="control-label label-color" >Upload DOB Proof <span className="color-red"> *</span></label>
                      </div>
                     
                      <div className="col-sm-4 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            {/* {this.state.isUploading &&
                                    <p>Progress: {this.state.dlProgress}</p>
                                  } */}
                            <div className="list-styling">
                              {this.displayList()}
                            </div>
                            <div><span style={{ fontSize: 12 }}>Please upload proof like DOB certificate, Passport, etc.</span></div>
                            <label style={{ backgroundColor: '#1D88D4', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                              Select File
                              <FileUploader
                                hidden
                                accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                                name="attachments"
                                storageRef={this.state.docRef}
                                onUploadStart={this.handleUploadStart}
                                onUploadError={this.handleUploadError}
                                onUploadSuccess={this.handleUploadSuccess}
                                onProgress={this.handleProgress}
                              />
                            </label>
                            <Indicator show={this.state.isUploading} />
                          </div>
                        </div>
                      </div>
                    {/* Documents */}

              {/* Gender */}
              <div className="col-sm-12">
                <label className="control-label label-color">Gender <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <Radio theKey="gender"  required="true" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />
                  </div>
                </div>
              </div>
              {/* Gender */}
              {/* Phone */}
              <div className="col-sm-12">
                <label className="control-label label-color">Phone <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Phone */}
              {/* Email */}
              <div className="col-sm-12">
                <label className="control-label label-color">Email <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Email */}
              {/* Parents name */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Name <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parents name */}
              {/* Parent contact number */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Contact Number</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="parent_mobile_number" value={this.state.parent_mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parent contact number */}
              {/* city */}
              <div className="col-sm-12">
                <label className="control-label label-color">City</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text"  name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* city */}
              {/* Club/School */}
              <div className="col-sm-12">
                <label className="control-label label-color">Club/School<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <ClubSelect theKey="club_name" updateAction={this.updateAction} value={this.state.club_name} options={this.state.clubList}/>
                  </div>
                </div>
              </div>
              {/* Club/School */}
              {/* Enter Club name manually */}
                {this.state.club_name == "Others" && <div>
                <div className="col-sm-12">
                <label className="control-label label-color">Enter Your club/school name<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  <input type="text"  name="other_club_name" aria-required="true" value={this.state.other_club_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
                  </div>}
              {/* Enter Club name manually */}
              {/* Group and Gender Details*/}
              {this.state.swimGroup != "" && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> {this.state.swimGroup}</label>
                </div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> Select Events</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="events" updateAction={this.updateAction} value={this.state.events} options={this.state.eventList} />
                    </div>
                  </div>
                </div>
                 {/* Timings for each events */}
              {this.state.selectedEvents.length >= 3 && <div>
                <div className="col-sm-12">
                  <label className="control-label color-red">You can only choose upto 2 choice(s).</label>
                </div>
                </div>}
              {(this.state.selectedEvents.length > 0 && this.state.selectedEvents.length < 3) && <div>
               {(this.state.swimGroup != "Group V" && this.state.swimGroup != "Group VI") && <div> <div className="col-sm-12">
                  <label className="control-label label-color">Enter Timing for Selected Events </label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      {this.getInputFields()}
                    </div>
                  </div>
                </div>
                </div>}
                </div>}
              {/* Timings for each events */}
                <div className="col-sm-12">
                  <label className="control-label label-color"> Select Relay Event</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="relay_event" updateAction={this.updateAction} value={this.state.relay_event} options={this.state.relayEventList} />
                    </div>
                  </div>
                </div>
                {/* Relay Members */}
               {this.state.selectedRelayEvent.length > 0 && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color">Enter your Relay Team Members</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <input type="text"  name="relay_team" aria-required="true" value={this.state.relay_team} onChange={this.handleInputChange} className="form-control" />
                      <span>4x50m Mixed Freestyle Relay- To take part in the mixed freestyle relay you must have four participants in each team, each team uses two boys and two girls in any order. Each team can enter no more than two relay teams in this event.</span>
                    </div>
                  </div>
                </div>
                </div>}
              {/* Relay Members */}
              </div>}
              {/* Group and Gender Details*/}
              {/* Payment */}
              <div className="col-sm-12">
                <label className="control-label label-color">Registration Fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.amount_to_pay} </p>
                  </div>
                </div>
              </div>
              {/* Payment */}
              {/* Terms and Conditions */}
              <div className="col-sm-12 ">
                <label className="control-label label-color">Terms and Conditions <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-10">
                <div className="border-height">
                <ol className="padding-left-25">
                  <label className="control-label label-color margin-tb-20">Eligibility Criteria </label>
                  <li>Swimmers will not be allowed to take part if he/she is a medallist in an individual event at any State , National and
                     International meet conducted by FINA, Swimming Federation of India (SFI), Delhi Swimming Association (DSA), School Games 
                     Federation of India, Department of Youth Services & Sports (DYSS) and CBSE  nationals till the date of the competition. 
                     This is an open invitational championship open to all swimmers (ALL SWIMMERS ARE WELCOME).</li>
                  <li>Swimmers who have won medals in the relay events at the State championships will be allowed to take part in this meet.</li>
                  <li>The competition will be held according to the FINA Rules.</li>
                  <li>Entry Fees of Rs. 600 per swimmer and Rs. 1000 /- per relay team 
                    (each swimmer of the relay team will pay Rs.250/- while registering individually) 
                    shall be paid online at the following link. Delhi Government Sarvodya Schools students no fee, 
                    but participants must submit hard copy of entry forms sign by the head of the school.
                    <b> On Spot Entries </b>: Deck entries will be accepted by the Organizing Secretary only. 
                    The fee will be Rs. 1000 payable immediately. Any deck-seeded entries must be approved by the 
                    Organizing Secretary a minimum of 60 minutes before the start of the event will be swim in.</li>
                  <li>Names of the Competitors, Date of Birth, Age Group and details of the events along with their timings is mandatory while filing entries online.</li>
                  <li>Birth certificate is mandatory for age proof. Necessary action will be taken against those who furnish incorrect information and/or certificate in regard to Age etc.</li>
                  <li>Swimmers will be eligible to take part in maximum of the 2 events excluding relays. A school/ club/ academy can enter a maximum numbers of competitors for any individual event and three teams for relay event.</li>
                  <li>Events will be held only on a“TIMED FINALS” basis.</li>
                  <li>Swimming events will be conducted with a“ONE START” rule.</li>
                  <li>Decision of the Referee is Final and the organisers shall not entertain any protests.</li>
                  <li>Last date for the submission of the entry is 10PM 14th August, 2018.<b> LATE ENTRIES WILL NOT BE ACCEPTED.</b></li>
                  <li>All the participants will receive a participation certificate and a Speedo gift voucher of Rs. 300 off on purchase of Rs. 1000.Medals and certificates will be awarded to all the winners.</li>
                </ol>

                <ul>
                <label className="control-label label-color margin-tb-20">Send offline entries and fees to below addresses: </label>
                  <li><b>Chand Tokas</b> at Delhi Public School, R. K. Puram, Sector-12, New Delhi-110022, Phone no. +91 9999887435</li>  
                  <li><b>Jitender/ Somender Tokas</b> at 249/F Munirka Village, New Delhi- 110022, Phone no. +91-7065195811, 9810952999</li>
                  <li><b>Vijay Kajla</b> at Venkateshwar Global School, Sector -13, Rohini, New Delhi-110085, Phone no. +91-9873226163</li>
                  <li>Last date for the submission of the entry is 10PM 14th August, 2018. <b>LATE ENTRIES WILL NOT BE ACCEPTED.</b></li>
                </ul>

                <ul>
                <label className="control-label label-color margin-tb-20">Obligations: </label>
                  <li>Every participant shall make sure that his/her health is in a condition that allows him/her to take part in the event, and consult a doctor if necessary. However, a basic FIRST AID medical facility will be provided in case of any emergency/ injuries.</li>
                  <li>He/she shall have proper swimsuit. Participants are supposed to maintain cleanness at the Stadium and do not cause any damages to the Government premises. Any loss/damage to infrastructure of Dr. SPM Swimming Pool Complex during the championship would be recovered by the person.</li>
                  <li>A participant must comply with instructions and directions given by the Organizer, the officials and race referees/ coaches. Referees decision will be final for all race categories.</li>
                  <li>Parents are not allowed to come on deck while any swimming event is going on. They should occupy stadium spectator’s seats.</li>
                  <li>Please note that the registration fee mentioned in the participation entry form will not be refunded if the candidate fails to participate in the event.</li>
                </ul>
                </div>
              </div>
              <div className="col-sm-12 margin-tb-20  ">
                <input className="margin-5" type="checkbox" required="true" name="agree_TC" aria-required="true" value={this.state.agree_TC} onChange={this.handleInputChange}/>By clicking here , I agree to the terms & conditions 
              </div>
              {/* Terms and Conditions */}
            </div>
            <div className="row margin-top-25px">
                  <div className="col-sm-12 text-align-center">
                    <button className="btn btn-size font-size" value="Save">Submit</button>
                  </div>
                </div>
          </form>
          </div>
            {/* Form */}
          </div>}
          {/* Main Form */}
          {/* thankyou Page */}
          {this.state.thankyou == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="row">
                <div className="col-sm-12 details-padding">
                  <p>Dear <b>{this.state.full_name} ,</b></p>
                  <p>Thank you for registering to <b> {this.state.meet_name}</b></p>
                  <h4> <b>Registration Details: </b></h4>
                  {/* Swimmser name */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Swimmer Name : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.full_name}</p>
                    </div>
                  </div>
                  {/* Swimmer name */}
                  {/* Group */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Group : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.swimGroup}</p>
                    </div>
                  </div>
                  {/* Group */}
                  {/* Events Selected */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Events Selected : </label>
                    </div>
                    <div className="col-sm-10">
                      <ol className="padding-l-15">
                          {this.displayRegisteredEvents()}
                          
                      </ol>
                    </div>
                  </div>
                  {/* Events Selected */}


                  {/* Amount to be paid */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Amount To Be Paid : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> Rs. {this.state.amount_to_pay}</p>
                    </div>
                  </div>
                  <p className="color-red">Your registration will be confirmed only after payment is completed.</p>
                  {/* Amount to be paid */}
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                     <button onClick={this.payOnline} className="btn btn-size font-size" value="Save">Pay Online</button>
                    </div>
                  </div>
                </div>
              </div>
           </div>
          </div>}
          {/* Thankyou Page */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p>
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>{this.state.meet_name},</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
      </div>
    )
  }
}
export default App;
