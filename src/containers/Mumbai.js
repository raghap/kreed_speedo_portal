import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import ClubSelect from '../components/fields/ClubSelect.js';
import CheckBox from '../components/fields/CheckBox.js';
import Radio from '../components/fields/Radio.js'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import FileUploader from 'react-firebase-file-uploader';
import Indicator from '../components/Indicator'
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

// for file uploader
const uuidv1 = require('uuid/v1');

var events = [
  {num:1,name:"50m Freestyle",group:"Group V",gender:"male"},
  {num:2,name:"50m Freestyle",group:"Group V",gender:"female"},
  {num:3,name:"50m Freestyle",group:"Group IV",gender:"male"},
  {num:4,name:"50m Freestyle",group:"Group IV",gender:"female"},
  {num:5,name:"50m Freestyle",group:"Group III",gender:"male"},
  {num:6,name:"50m Freestyle",group:"Group III",gender:"female"},
  {num:7,name:"50m Freestyle",group:"Group II",gender:"male"},
  {num:8,name:"50m Freestyle",group:"Group II",gender:"female"},
  {num:9,name:"50m Freestyle",group:"Group I",gender:"male"},
  {num:10,name:"50m Freestyle",group:"Group I",gender:"female"},
  {num:11,name:"50m Back Stroke",group:"Group V",gender:"male"},
  {num:12,name:"50m Back Stroke",group:"Group V",gender:"female"},
  {num:13,name:"50m Back Stroke",group:"Group IV",gender:"male"},
  {num:14,name:"50m Back Stroke",group:"Group IV",gender:"female"},
  {num:15,name:"50m Back Stroke",group:"Group III",gender:"male"},
  {num:16,name:"50m Back Stroke",group:"Group III",gender:"female"},
  {num:17,name:"50m Back Stroke",group:"Group II",gender:"male"},
  {num:18,name:"50m Back Stroke",group:"Group II",gender:"female"},
  {num:19,name:"50m Back Stroke",group:"Group I",gender:"male"},
  {num:20,name:"50m Back Stroke",group:"Group I",gender:"female"},
  {num:21,name:"25m Freestyle",group:"Group VI",gender:"male"},
  {num:22,name:"25m Freestyle",group:"Group VI",gender:"female"},
  {num:23,name:"4*50mts FS Relay",group:"Group V",gender:"male"},
  {num:24,name:"4*50mts FS Relay",group:"Group V",gender:"female"},
  {num:25,name:"4*50mts FS Relay",group:"Group IV",gender:"male"},
  {num:26,name:"4*50mts FS Relay",group:"Group IV",gender:"female"},
  {num:27,name:"50m Butterfly",group:"Group V",gender:"male"},
  {num:28,name:"50m Butterfly",group:"Group V",gender:"female"},
  {num:29,name:"50m Butterfly",group:"Group IV",gender:"male"},
  {num:30,name:"50m Butterfly",group:"Group IV",gender:"female"},
  {num:31,name:"50m Butterfly",group:"Group III",gender:"male"},
  {num:32,name:"50m Butterfly",group:"Group III",gender:"female"},
  {num:33,name:"50m Butterfly",group:"Group II",gender:"male"},
  {num:34,name:"50m Butterfly",group:"Group II",gender:"female"},
  {num:35,name:"50m Butterfly",group:"Group I",gender:"male"},
  {num:36,name:"50m Butterfly",group:"Group I",gender:"female"},
  {num:37,name:"50m Breast Stroke",group:"Group V",gender:"male"},
  {num:38,name:"50m Breast Stroke",group:"Group V",gender:"female"},
  {num:39,name:"50m Breast Stroke",group:"Group IV",gender:"male"},
  {num:40,name:"50m Breast Stroke",group:"Group IV",gender:"female"},
  {num:41,name:"50m Breast Stroke",group:"Group III",gender:"male"},
  {num:42,name:"50m Breast Stroke",group:"Group III",gender:"female"},
  {num:43,name:"50m Breast Stroke",group:"Group II",gender:"male"},
  {num:44,name:"50m Breast Stroke",group:"Group II",gender:"female"},
  {num:45,name:"50m Breast Stroke",group:"Group I",gender:"male"},
  {num:46,name:"50m Breast Stroke",group:"Group I",gender:"female"},
  {num:47,name:"25m Kickboard",group:"Group VI",gender:"male"},
  {num:48,name:"25m Kickboard",group:"Group VI",gender:"female"},
  {num:49,name:"4*50mts FS Relay",group:"Group III",gender:"male"},
  {num:50,name:"4*50mts FS Relay",group:"Group III",gender:"female"},
  {num:51,name:"4*50mts FS Relay",group:"Group II",gender:"male"},
  {num:52,name:"4*50mts FS Relay",group:"Group II",gender:"female"},
  {num:53,name:"4*50mts FS Relay",group:"Group I",gender:"male"},
  {num:54,name:"4*50mts FS Relay",group:"Group I",gender:"female"}
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name:"Speedo Invitational Swimming Championship 2018, Mumbai",
      Kreed_ID:"",
      full_name: "",
      second_name: "",
      date_of_birth: "",
      gender: "",
      mobile_number: "",
      email: "",
      parent_name: "",
      parent_mobile_number: "",
      city: "",
      club_name: "",
      clubList: ["Andheri Recreation Club","Andheri Sports Complex","Bharat Petroleum Corporation Ltd (Refinery) Sports Club",
                "Billabong High","Body Rhythm Club","Bombay Gymkhana Ltd","Bombay Presidency Radio Club",
                "Breach Candy Swimming Bath Trust","Celebration Sports Club","Central Railway Sports Association",
                "Chembur Gymkhana","Club Millenium","Garware Club House","General Arun Kumar Vaidya Municipal Swimming Pool",
                "Ghatkopar Jolly Gymkhana","Ghatkopar Lions Municipal Swimming Pool","Glenmark Aquatic Foundation",
                "Goenka & Associates Educational Trust","Hiranandani Swimmers Club","Indian Institute Of Technology Students Gymkhana",
                "Juhu Vile Parle Gym Club","Kamgar Kreeda Kendra Swimming Pool","Kandivali Recreation Club",
                "Karmaveer Sports Complex","Khar Gymkhana","Lawrence High School","M.I.G Cricket Club",
                "Malbar Hill Club","Mandapeshwar Civic Federation","Matunga Gymkhana","Mgmo Swimming Pool",
                "Mulund Swimming Pool","Murbalidevi Swimming Pool & Gymnasium","National Sports Club Of India",
                "Otters Club","Ozone Swimming Pool & Club","Police Swimming Pool","Prabhodankar Thakre Swimming Pool",
                "Pransuklal Mafatlal Hindu Swimming Bath & Boat Club","Rashtriya Chemical & Fertilizers Sports Club",
                "Renaissance Federation Club","Ryan International Sports Club","S.V.P Swimming Pool",
                "Sharada Mandir High School","Sher-E-Punjab Gymkhana","The Acres Lifestyle Club",
                "The Air India Sports Club","The Classique Club","The Club","The Cricket Club Of India",
                "The Goregaon Sports Club","The Indian Gymkhana (Matunga)","The Jankidevi Public School",
                "The S.G.N. Sports Club","The Willingdoncatholic Gymkhana","Trombay Swimming Pool",
                "Vibgyor High","Western Railway Sports Association","Willingdon Sports Club","YMCA",
                "Ahmadnagar","Akola","Amravati","Aurangabad","Bhandara","Bid","Buldana","Chandrapur",
                "Dhule","Gadchiroli","Gondiya","Hingoli","Jalgaon","Jalna","Kolhapur","Latur","Mumbai City",
                "Mumbai Suburban","Nagpur","Nanded","Nandurbar","Nashik","Osmanabad","Parbhani",
                "Pune","Raigarh","Ratnagiri","Sangli","Satara","Sindhudurg","Solapur","Thane","Wardha",
                "Washim","Yavatmal","Other"],
      other_club_name:"",
      swimGroup: "",
      athleteInfo: "",
      amount_to_pay: 0,
      events:"",
      eventList: [],
      selectedEvents:[],
      selectedEventTimings:[],
      selectedEventNumbers:[],
      currentEvent:[],
            
      agree_TC: "",
      selectedClubValue:"",
      isLoading: false,
      errorAlert: false,
      // athleteLoadingError: false,
      athleteObj:{},
      newKreedID:"",
      enrollmentID:"",
      // Navigation for different pages
      register: "Yes",
      thankyou: "No",
      paymentSuccess: "No",
      // Documents
      docURL: "",
      isUploading: false,
      dlProgress: new Map()
    }

    var strMeetName = this.state.meet_name.replace(/[^A-Z0-9]+/ig, "_");
    this.state.docRef = firebase.app.storage().refFromURL('gs://kreed-of-sports-speedo/' + strMeetName + "/" + uuidv1());
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.getGroup = this.getGroup.bind(this);
    this.getAltGroup = this.getAltGroup.bind(this);
    this.getEventList = this.getEventList.bind(this);
    this.resetdata = this.resetdata.bind(this);
    this.submitRegistration = this.submitRegistration.bind(this);
    this.capturePayment = this.capturePayment.bind(this);
    this.getAmountToPay = this.getAmountToPay.bind(this);
    this.getselectedEventNumbers = this.getselectedEventNumbers.bind(this);
    this.checkEv = this.checkEv.bind(this);
    this.displayRegisteredEvents = this.displayRegisteredEvents.bind(this);
    this.payOnline = this.payOnline.bind(this);
   //for displaying docs list
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);
  }

  // for diplaying file list
  handleUploadStart(fname, task) {
    this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
    if (!this.state.isUploading)
      this.setState({ isUploading: true, progress: 0 });
  }
  handleProgress(progress, task) {
    if (progress <= 100) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
      this.setState({ avatarURL: task.snapshot.ref.fullPath });
    }
  }
  handleUploadError(error) {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess(filename, task) {
    // this.setState({avatar: filename, progress: 100, isUploading: false})
    this.state.dlProgress.delete(task.snapshot.ref.fullPath);
    if (this.state.dlProgress.size == 0)
      this.state.isUploading = false;
      this.state.docRef.child(filename).getDownloadURL()
      .then(url => {
        this.setState({ docURL: url });
      })
      .catch(error => {
        alert("err "+error)
      })
  };

   //displaying file names
   displayList() {
    var i;
    var items = [];

    var link = this.state.docURL;
    var pts = link.split('/');
    var fn = unescape(pts[pts.length - 1].split('?')[0]);
    var fnparts = fn.split('/');
    items.push(fnparts[2]);
    
    return items;
  }

  resetdata() {
    var newState = {};
      newState.full_name = "";
      newState.second_name = "";
      newState.date_of_birth = "",
      newState.gender = "",
      newState.mobile_number = "",
      newState.email = "",
      newState.parent_name = "",
      newState.parent_mobile_number = "",
      newState.events = "",
      newState.city = "",
      newState.club_name = "",
      newState.selectedEvents = [],
      newState.selectedEventNumbers = [],
      newState.ksa_id = "",
      newState.eventList = [],
      newState.selectedClubValue = ""
    this.setState(newState);

  }
  getEventList() {
    var events = [];

    if (this.state.swimGroup == "Group I" || this.state.swimGroup == "Group II" || 
        this.state.swimGroup == "Group III" || this.state.swimGroup == "Group IV" ||
        this.state.swimGroup == "Group V" ) 
    {
      events = ["50m Freestyle", "50m Back Stroke","50m Breast Stroke","50m Butterfly"];
    } 
    else if (this.state.swimGroup == "Group VI") 
    {
      events = ["25m Freestyle", "25m Kickboard"];
    } 

    this.setState({ eventList: events })
  }
  //to get group based on today date
  getAltGroup(dob) {
    var timestamp = Date.parse(dob);
// alert(dob)
    if (isNaN(timestamp))
      return "Not Permitted";
      var gp1_lower = new Date("2001-07-01 00:00:00.000").getTime();
      var gp1_upper = new Date("2004-06-30 23:59:59.999").getTime();

      var gp2_lower = new Date("2004-07-01 00:00:00.000").getTime();
      var gp2_upper = new Date("2006-06-30 23:59:59.999").getTime();

      var gp3_lower = new Date("2006-07-01 00:00:00.000").getTime();
      var gp3_upper = new Date("2008-06-30 23:59:59.999").getTime();
      
      var gp4_lower = new Date("2008-07-01 00:00:00.000").getTime();
      var gp4_upper = new Date("2010-06-30 23:59:59.999").getTime();

      var gp5_lower = new Date("2010-07-01 00:00:00.000").getTime();
      var gp5_upper = new Date("2012-06-30 23:59:59.999").getTime();
      
      var gp6_lower = new Date("2012-07-01 00:00:00.000").getTime(); 
      var gp6_upper = new Date("2014-06-30 23:59:59.999").getTime();

      var dob_ms = dob.getTime();
      var ret = "You must be born between 01/07/2001 and 30/06/2014 to participate";
// alert("DOB" + dob_ms);
      if(dob_ms >= gp1_lower && dob_ms <= gp1_upper)
                  ret = "Group I";
      else if(dob_ms >= gp2_lower && dob_ms <= gp2_upper)
                  ret = "Group II";
      else if(dob_ms >= gp3_lower && dob_ms <= gp3_upper)
                  ret = "Group III";
      else if(dob_ms >= gp4_lower && dob_ms <= gp4_upper)
                  ret = "Group IV"
      else if(dob_ms >= gp5_lower && dob_ms <= gp5_upper)
                  ret = "Group V";
      else if(dob_ms >= gp6_lower && dob_ms <= gp6_upper)
                  ret = "Group VI";

      return ret;
  }

  // to get group
  getGroup(dob) {
    var timestamp = Date.parse(dob);

    if (isNaN(timestamp))
      return "Open";

    var current = new Date();
    var dob_dt = new Date(dob);
    var today = current.getFullYear() - dob_dt.getFullYear();

    if (today > 17) {
      return "Ages above 17 are not permitted";
    } else if (today == 17 || today == 16 || today == 15) {
      return "Group I";
    } else if (today == 14 || today == 13) {
      return "Group II";
    } else if (today == 12 || today == 11) {
      return "Group III";
    } else if (today == 10 || today == 9) {
      return "Group IV";
    } else if (today == 8 || today == 7) {
      return "Group V";
    } else if (today == 6 ) {
      return "Group VI";
    } else {
      return "Kids below 4 years of age are not permitted."
    }
  }
  componentDidMount() {

  }
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "full_name") {
      if (!value.match(/^[ a-zA-Z.]+$/) && value != '') {
        return false;
      }
    }

    if (name == "other_club_name") {
      if (!value.match(/^[ a-zA-Z.']+$/) && value != '') {
        return false;
      }
    }

    if (name == "parent_name") {
      if (!value.match(/^[ a-zA-Z.]+$/) && value != '') {
        return false;
      }
    }
   
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
    if (name == "parent_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
   

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }

    if(name.startsWith("eventTiming")){
      var index = name.split("_")[1];
      var timings = this.state.selectedEventTimings;
      timings[index] = value;
      this.setState({selectedEventTimings: timings});
      return;
    }

    this.setState({
      [name]: value
    });
    // alert(this.state.events);
  }
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/assoc/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {

        if (key == "date_of_birth") {
          this.state.selectedEvents = "";
          this.state.events = "";
          // this.state.swimGroup = this.getGroup(value);
          this.state.swimGroup = this.getAltGroup(value);
          // alert(this.sate)
          this.getEventList();
        }

        //Checkbox handling
        if(key == "events"){
        
          if(typeof(value) == "string")
            this.state.selectedEvents = value.slice(1).split(",");
          else
          {
            this.state.selectedEvents = [];
            value = "";
          }
          this.getAmountToPay();
          if(this.state.gender!=""){
            this.getselectedEventNumbers();
          }
          else{
            this.state.errorStatement = "Please ensure you have selected a gender";
            this.setState({errorAlert: true})
            this.getselectedEventNumbers();
          }
          
        }
       
        this.setState({ [key]: value });
      }

    }
  }
  checkEv(ev) {
    // return (ev.group=="Group I" && ev.gender=="male" && ev.name=="50M Freestyle");
    return (ev.group == this.state.swimGroup && ev.gender == this.state.gender && ev.name==this.state.currentEvent);
  }

  getselectedEventNumbers(){
    var i;
    var order = [];
    for (i = 0; i < this.state.selectedEvents.length; i++) { 
      this.state.currentEvent = this.state.selectedEvents[i];
      var evt = events.filter(this.checkEv);
      order.push(evt[0].num)
    }
    this.state.selectedEventNumbers = order;
  }
  getAmountToPay(){
    var i;
    for (i = 0; i < this.state.selectedEvents.length; i++) {
      this.state.amount_to_pay = 100 * (i+1);
    }
  }
  
  displayRegisteredEvents(){
    var i;
    var items = [];

    for (i = 0; i <  this.state.selectedEvents.length; i++) {
      items.push(<li>{this.state.selectedEvents[i]}</li>);
    }
    return items;
  }



  getDataObject()
  {
      var dObj = {};

        if(this.state.city == ""){
          this.state.city = "Mumbai";
        }

        dObj.full_name = this.state.full_name;
        dObj.date_of_birth = this.state.date_of_birth;
        dObj.gender = this.state.gender;
        
        dObj.parent_name = this.state.parent_name;
        dObj.swimGroup = this.state.swimGroup;
        dObj.city = this.state.city;
        dObj.country = "India";

        //common fields
        dObj.mobile_number = this.state.mobile_number;
        dObj.email = this.state.email;
        dObj.parent_mobile_number = this.state.parent_mobile_number;
        dObj.club_name = this.state.club_name;
        dObj.amount = this.state.amount_to_pay;

        //event regn fields - always carried.
        dObj.brand_name = "speedo";
        dObj.meet_name = this.state.meet_name;
        dObj.selected_events = this.state.selectedEvents.slice();
        dObj.selected_event_timings = this.state.selectedEventTimings.slice();
        dObj.selected_event_numbers = this.state.selectedEventNumbers.slice();
        dObj.document_path = this.state.docURL;

    return dObj;

  }

  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.state.newKreedID, enrollment_id: this.state.enrollmentID, payment_id: payResp.razorpay_payment_id, brand_name: "speedo", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true ,thankyou: "No",paymentSuccess: "Yes"  });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }

  payOnline(){
    var options = {
      "key": "rzp_live_SKW6ZcvymAaWBJ",
      "amount": this.state.amount_to_pay * 100 , // 2000 paise = INR 20
      "name": "SwimIndia",
      "description": "Speedo Invitational Swimming Championship 2018, Mumbai",
      // "image": "/your_logo.png",
      "handler": this.capturePayment,
      "prefill": {
          "contact": this.state.mobile_number,
          "email": this.state.email,
      },
      "notes": {
          "address": this.state.city
      },
      "theme": {
          "color": "#528FF0"
      }
    };
    var rzp = new window.Razorpay(options);
    rzp.open();

  }
  submitRegistration(e)
  {
      e.preventDefault();
      if(this.state.isUploading){
        this.state.errorStatement = "Please wait Uploading your document";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.date_of_birth == "" ){
        this.state.errorStatement = "Select Your Date Of Birth";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.docURL == "" ){
        this.state.errorStatement = "Please upload your DOB proof";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.gender === "" ){
        this.state.errorStatement = "Please ensure you have selected a gender";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.mobile_number.length < 10 ){
        this.state.errorStatement = "Please ensure you have entered a 10 digit mobile number";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == ""){
        this.state.errorStatement = "Select Your Club";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == "Other" && this.state.other_club_name == ""){
        this.state.errorStatement = "Enter Your Club name";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length < 1 ){
        this.state.errorStatement = "Select atleast one event";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == "Other"){
        this.state.club_name = this.state.other_club_name
      }
      this.setState({ isLoading: true });
      axios({
        method: 'post',
        headers:  {
                      'Content-Type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                  },
        url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitMeetRegistration',
        data: this.getDataObject()
      })
      .then(response => {
        // alert("KreedId" + response.data.kreedID + ", Enrolment Id: " + response.data.enrollmentID);
        this.state.newKreedID = response.data.kreedID;
        this.state.enrollmentID = response.data.enrollmentID;
        // Navigating to thankyou PAGE
        this.setState({register: "No" , thankyou: "Yes",isLoading: false });
      })
      .catch(error => {
        this.state.errorStatement = "Error completing registration";
        this.setState({ isLoading: false, errorAlert: true })
        // console.log("Error submitting athlete details: ", error);
      })
 }


//Container Render Method
render() {
  return (
    <div className="content">
       <img alt="registration are closed" src='assets/img/registration_closed_mumbai.jpg' />
    </div>
  )
}

  render_unused(){
    return (
      
      <div className="content">
      {/* SWEET ALERTS */}
      <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
                <img className="banner-img-styling" src='assets/img/Speedo_Invitational_Swimming_Championship_2018_Mumbai_wallpaper.png' />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
                <img className="wrapper-img-styling" src='assets/img/Speedo_Invitational_Swimming_Championship_2018_Mumbai_wrapper.png' />
                <h2>{this.state.meet_name}</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Main Form */}
          {this.state.register == "Yes" && <div> <div className="row whitebg border padding-20">
            {/* Address */}
             <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <p className="margin-0px"><b>Venue: </b>Tata Power Swimming Pool, Tata Electric Officers Colony, Aziz Baug, Chembur, Mumbai - 400074</p>
                <p className="margin-0px"><a target="_blank" className="linkStyling text-color" href="https://goo.gl/maps/L7idSrZxuJG2"><b>Location Map</b></a></p>
                <p className="margin-0px"><b>Dates: </b> October 21st, 2018</p>
                <p className="margin-0px"><b>Reporting Time: </b>6:30am </p>
                <p className="margin-0px"><b>Competition Start Time: </b>7:30am </p>
                Visit <a target="_blank" className="linkStyling" href="http://swimindia.in/speedo-invitational-swimming-championship-2018-mumbai-maharashtra">SwimIndia</a> to know more about the meet.
              </div>
            </div>
            {/* Address */}
            {/* Form */}
            <form onSubmit={this.submitRegistration}>
            <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <label className="control-label label-color">Swimmer Name <span className="color-red"> *</span></label>
              </div>
              {/* First Name */}
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* First Name */}
              {/* Date of birth */}
              <div className="col-sm-12">
                <label className="control-label label-color">Date Of Birth <span className="color-red"> *</span></label>
                <div><span style={{ fontSize: 12 }}>Please select your DOB to view Groups and Events</span></div>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                     <KreedDatePicker theKey="date_of_birth" required="true" value={this.state.date_of_birth} updateAction={this.updateAction} />
                  </div>
                </div>
              </div>
              {/* Date of birth */}
                    {/* Documents */} 
                      <div className="col-sm-12">
                        <label className="control-label label-color" >Upload DOB Proof <span className="color-red"> *</span></label>
                      </div>
                     
                      <div className="col-sm-4 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            {/* {this.state.isUploading &&
                                    <p>Progress: {this.state.dlProgress}</p>
                                  } */}
                            <div className="list-styling">
                              {this.displayList()}
                            </div>
                            <div><span style={{ fontSize: 12 }}>Please upload proof like DOB certificate, Passport, etc.</span></div>
                            <label style={{ backgroundColor: '#1D88D4', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                              Select File
                              <FileUploader
                                hidden
                                accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                                name="attachments"
                                storageRef={this.state.docRef}
                                onUploadStart={this.handleUploadStart}
                                onUploadError={this.handleUploadError}
                                onUploadSuccess={this.handleUploadSuccess}
                                onProgress={this.handleProgress}
                              />
                            </label>
                            <Indicator show={this.state.isUploading} />
                          </div>
                        </div>
                      </div>
                    {/* Documents */}

              {/* Gender */}
              <div className="col-sm-12">
                <label className="control-label label-color">Gender <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <Radio theKey="gender"  required="true" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />
                  </div>
                </div>
              </div>
              {/* Gender */}
              {/* Phone */}
              <div className="col-sm-12">
                <label className="control-label label-color">Phone <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Phone */}
              {/* Email */}
              <div className="col-sm-12">
                <label className="control-label label-color">Email <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Email */}
              {/* Parents name */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Name <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parents name */}
              {/* Parent contact number */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Contact Number</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="parent_mobile_number" value={this.state.parent_mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parent contact number */}
              {/* city */}
              <div className="col-sm-12">
                <label className="control-label label-color">City</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text"  name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* city */}
              {/* Club/School */}
              <div className="col-sm-12">
                <label className="control-label label-color">Club/School<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <ClubSelect theKey="club_name" updateAction={this.updateAction} value={this.state.club_name} options={this.state.clubList}/>
                  </div>
                </div>
              </div>
              {/* Club/School */}
              {/* Enter Club name manually */}
              {this.state.club_name == "Other" && <div>
                <div className="col-sm-12">
                <label className="control-label label-color">Enter Your club/school name<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text"  name="other_club_name" aria-required="true" value={this.state.other_club_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
                  </div>}
              {/* Enter Club name manually */}
              {/* Group and Gender Details*/}
              {this.state.swimGroup != "" && <div>
                <div className="col-sm-12">
                {this.state.swimGroup.startsWith("Group") && 
                  <label className="control-label label-color"> {this.state.swimGroup}</label>
                }
                {!this.state.swimGroup.startsWith("Group") && 
                  <label className="control-label label-color color-red"> {this.state.swimGroup}</label>
                }
                </div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> Select Events</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="events" updateAction={this.updateAction} value={this.state.events} options={this.state.eventList} />
                    </div>
                  </div>
                </div>
              </div>}
              {/* Group and Gender Details*/}
              {/* Payment */}
              <div className="col-sm-12">
                <label className="control-label label-color">Registration Fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.amount_to_pay} </p>
                  </div>
                </div>
              </div>
              {/* Payment */}
              {/* Terms and Conditions */}
              <div className="col-sm-12 ">
                <label className="control-label label-color">Terms and Conditions <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-10">
                <div className="border-height">
                <ol className="padding-left-25">
                  <li>Swimmers will not be allowed to take part if he/she is a medallist in an individual event at any State, National and International meet.</li>
                  <li>Swimmers who have won medals in the relay events at the State, National & International championships will be allowed to take part in this meet.</li>
                  <li>The competition will be held according to the FINA Rules.</li>
                  <li>Entry Fees of Rs. 100 per individual Events shall be paid online at the following link https://speedo.kreedofsports.com/#/mumbai</li>
                  <li>For Relay entries, please send the relay team entries (Full Name, Date of Birth, Name of Club/School and Event Name) to registrations@swimidnia.in and you will get a SMS for online payment. Relay team registrations will be confirmed only after the payment is done.</li>
                  <li><b>Spot Entries:</b> The fee will be Rs. 200 per Individual Event and Rs.200 per person for Relay events
                    payable immediately. Spot entries will be accepted and approved by only the Organizing Crew, a
                    minimum of 1 hour before the start of the event to be swum in.</li>
                  <li>Names of the Competitors, Date of Birth, Age Group and details of the events are mandatory during registration.</li>
                  <li>Birth certificate or a Proof of Age is mandatory.</li>
                  <li>Events will be held only on a <b>“TIME TRIAL”</b> basis.</li>
                  <li>Swimming events will be conducted with a <b>“ONE START”</b> rule.</li>
                  <li>Decision of the Referee is Final and the organisers shall not entertain any protests.</li>
                  <li>Last date for the submission of the online entries is <b>10PM on 18th October 2018</b> and offline
                      entries will be <b>15th October 2018</b></li>
                  <li>All the participants will receive a Participation Certificate and an Exclusive Speedo Gift
                    voucher of Rs. 300 off on purchase of Rs. 999, which can be redeemed at the Speedo kiosk set
                    up at the venue.</li>
                  <li>Medals and certificates will be awarded to first 3 place winners. Certificates will be awarded
                    to first 6 places. Swimmers with highest points in each group will receive an Individual
                    Championship Certificate and Goodies from Speedo.</li>
                  <li><b>Group 1 - 4</b> will be recognised as a State Level Swimming Championship. <b>Group 5 and 6</b>
                     category will be an Invitational Swimming Championship.</li>

                  {/* <label><b>For queries please email:</b> <a className="linkStyling" href="mailto:rajupalkar@gmail.com" >rajupalkar@gmail.com </a></label> */}
                </ol>
                </div>
              </div>
              <div className="col-sm-12 margin-tb-20  ">
                <input className="margin-5" type="checkbox" required="true" name="agree_TC" aria-required="true" value={this.state.agree_TC} onChange={this.handleInputChange}/>By clicking here , I agree to the terms & conditions 
              </div>
              {/* Terms and Conditions */}
            </div>
            <div className="row margin-top-25px">
                  <div className="col-sm-12 text-align-center">
                    <button className="btn btn-size font-size" value="Save">Submit</button>
                  </div>
                </div>
          </form>
          </div>
            {/* Form */}
          </div>}
          {/* Main Form */}
          {/* thankyou Page */}
          {this.state.thankyou == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="row">
                <div className="col-sm-12 details-padding">
                  <p>Dear <b>{this.state.full_name} ,</b></p>
                  <p>Thank you for registering to <b> {this.state.meet_name}</b></p>
                  <h4> <b>Registration Details: </b></h4>
                  {/* Swimmser name */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Swimmer Name : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.full_name}</p>
                    </div>
                  </div>
                  {/* Swimmer name */}
                  {/* Group */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Group : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.swimGroup}</p>
                    </div>
                  </div>
                  {/* Group */}
                  {/* Events Selected */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Events Selected : </label>
                    </div>
                    <div className="col-sm-10">
                      <ol className="padding-l-15">
                          {this.displayRegisteredEvents()}
                          
                      </ol>
                    </div>
                  </div>
                  {/* Events Selected */}


                  {/* Amount to be paid */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Amount To Be Paid : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> Rs. {this.state.amount_to_pay}</p>
                    </div>
                  </div>
                  <p className="color-red">Your registration will be confirmed only after payment is completed.</p>
                  {/* Amount to be paid */}
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                     <button onClick={this.payOnline} className="btn btn-size font-size" value="Save">Pay Online</button>
                    </div>
                  </div>
                </div>
              </div>
           </div>
          </div>}
          {/* Thankyou Page */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p>
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>{this.state.meet_name},</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
      </div>
    )
  }
}
export default App;
