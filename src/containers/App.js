import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name: "Speedo Invitational Swimming Championship 2018, Bangalore",
    }

  }


  //Container Render Method
  render() {
    return (
      <div>
      <div className="bgded overlay overlay-background" >
        <div id="pageintro" className="hoc clear">
          <a href="#">
            <div className="flexslider basicslider">
              <ul className="slides">
                <li>
                  <article>
                    <a href="#">   <img src="assets/img/landingpage/2300x1000-Generic.png" /> </a>
                  </article>
                </li>
                <li>
                  <article>
                  <Link to='/bangalore' target="_blank">    
                     <img src="assets/img/landingpage/2300x1000-BLR.jpg" />
                   </Link>
                  </article>
                </li>
                <li>
                  <article>
                  <Link to='/delhi' target="_blank"> 
                    <img src="assets/img/landingpage/2300x1000-Delhi.jpg" />
                  </Link>
                  </article>
                </li>
                <li>
                  <article>
                    <Link to='/mumbai' target="_blank"> 
                      <img src="assets/img/landingpage/2300x1000-mumbai.jpg" />
                    </Link>
                  </article>
                </li>
              </ul>
            </div>
          </a>
        </div>

        <div id="introblocks" className="hoc clear">
          <ul className="nospace">
            <li>
              <a href="#bangaloresection"><img src="assets/img/landingpage/bangaloremeet.png" /></a>
            </li>
            <li>
              <a href="#newdelhi"><img src="assets/img/landingpage/newdelhimeet.png" /></a>
            </li>
            <li>
              <a href="#mumbaisection"><img src="assets/img/landingpage/mumbaimeet.png" /></a>
            </li>
            {/* <li>
              <a href="#"><img src="assets/img/landingpage/othercities.png" /></a>
            </li> */}
          </ul>
        </div>
      </div>


      <div className="wrapper row3" id="bangaloresection">
        <main className="hoc container clear  padding-tb-50">
          <div className="btmspace-80 center">
            <p></p>

            <h6 className="heading">Speedo Invitational Swimming Championship 2018, Bangalore</h6>
            <p className="nospace">Nisha Millets Swimming Academy is pleased to invite swimmers of your Club/School to participate in the Speedo Invitational Swimming Championship 2018, on 4th and 5th of August 2018 at the Kensington Pool, Ulsoor.     </p>

          </div>
          <div className="clear">
            <div className="one_half first">
              <ul className="nospace group">
                <li className="one_half first">
                  <article><a href="#"><i className="fa fa-calendar icon btmspace-30 fa-font"></i></a>
                    <h6 className="heading font-x1">Venue & Date</h6>
                    <p>Kensington Pool, Ulsoor, Bangalore</p>
                    <p className="nospace">Dates: August 4th, 2018, Saturday – Group 4, Group 5 and Group 6</p><br/>
                      <p className="nospace">  August 5th 2018, Sunday – Group 1, Group 2 and Group 3, MASTERS</p>
                   </article>
                 </li>
                  <li className="one_half">
                    <article><a href="#"><i className="fa fa-info icon btmspace-30 fa-font"></i></a>
                      <h6 className="heading font-x1">Details</h6>
                      <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/07/Speedo-Meet-Circular-Bangalore.pdf" target="_blank">Meet Circular</a></p>
                      <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/07/Speedo-Meet-2018-Order-of-Events.pdf" target="_blank">Order of Events</a></p>
                      <p>   <a href="https://goo.gl/maps/kaZNTh1VTDo" target="_blank">Find Location on Google Maps</a></p>
                      <p>   <a href="http://swimindia.in/2nd-speedo-invitational-swimming-championship-2018-bangalore" target="_blank">Complete Meet Details</a></p>

                    </article>
                  </li>
        </ul>
      </div>
              <div className="one_half"><img className="inspace-10 borderedbox" src="assets/img/landingpage/480x400-Blr.jpg" alt="" /></div>
            </div>
            <div className="clear"></div>
  </main>
</div>


        <div className="wrapper row3">
          <div className="hoc container clear padding-t-0 padding-b-25">
            <ul className="nospace group center">
              <li className="one_quarter first">
                <article><a target="_blank" href="assets/pdf/Medal list Bangalore.pdf"><i className="fa fa-trophy	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1 ">Medals</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a href="#"><i className="fa fa-file-picture-o	 icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Gallery</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a target="_blank" href="assets/pdf/Individual-Championship-Bangalore-Speedo-Meet-2018.pdf"><i className="fa fa-child	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Individual Championship</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a target="_blank" href="assets/pdf/Results of Speedo Invitational Swimming Championship.pdf"><i className="fa fa-tasks	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Results</h6>
                </article>
              </li>
            </ul>
            <div className="clear"></div>
          </div>
        </div>
        <div className="wrapper bgded overlay bgded-image">
          <article className="hoc container clear center">
            <p className="font-x1">Speedo Invitational Swimming Championship 2018, Bangalore</p>
            <h2>Registrations are now closed</h2>
            {/* <h2 className="heading font-x3">Register Now & Pay Online</h2>
            <footer>
              <ul className="nospace inline pushright">
                <li><a className="btn" href="https://speedo.kreedofsports.com/#/bangalore" target="_blank">Register & Pay Online</a></li>
              </ul>
            </footer> */}
            <div className="clear"></div>
          </article>
        </div>


        <div className="wrapper row3" id="newdelhi">
          <main className="hoc container clear paading-tb-50">
            <div className="btmspace-80 center">
              <p></p>

              <h6 className="heading">Speedo BGN Invitational Swimming Championship 2018, New Delhi</h6>
              <p className="nospace">BGN Swimming Academy is pleased to invite the swimmers of your Club/School/Institute to participate in the Speedo-BGN Invitational Swimming Championship 2018, which is created by Speedo to encourage and cultivate home grown talent of all ages.  </p>

            </div>
            <div className="clear">
              <div className="one_half first">
                <ul className="nospace group">
                  <li className="one_half first">
                    <article><a href="#"><i className="fa fa-calendar icon btmspace-30 fa-font"></i></a>
                      <h6 className="heading font-x1">Venue & Date</h6>
                      <p>Venue: Dr. Shyama Prasad Mukherjee Swimming Pool Complex, New Delhi</p>
                      <p className="nospace">Date: 19th August, 2018</p><br/>
			  
            </article>
          </li>
                    <li className="one_half">
                      <article><a href="#"><i className="fa fa-info icon btmspace-30 fa-font"></i></a>
                        <h6 className="heading font-x1">Details</h6>
                        <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/07/SPEEDO-BGN-INVITATIONAL-SWIM-MEET-2018.pdf" target="_blank" >Meet Circular</a></p>
                        <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/07/Order-of-Events-2018.pdf" target="_blank" >Order of Events</a></p>
                        <p>   <a href="https://goo.gl/maps/egmYfzJrAYC2" target="_blank" >Find Location on Google Maps</a></p>
                        <p>   <a href="http://swimindia.in/speedo-bgn-invitational-swimming-championship-2018-delhi" target="_blank" >Complete Meet Details</a></p>

                      </article>
                    </li>	
        </ul>
      </div>
                <div className="one_half"><img className="inspace-10 borderedbox" src="assets/img/landingpage/480x400-Delhi.jpg" alt="" /></div>
              </div>
              <div className="clear"></div>
  </main>
</div>



          <div className="wrapper row3">
            <div className="hoc container clear padding-t-0 padding-b-25">
              <ul className="nospace group center">
                <li className="one_quarter first">
                  <article><a target="_blank" href="assets/pdf/MedalList-Speedo-BGN-Swimming-Championship.pdf" ><i className="fa fa-trophy	icon btmspace-15 fa-font"></i></a>
                    <h6 className="heading font-x1">Medals</h6>
                  </article>
                </li>
                <li className="one_quarter">
                  <article><a target="_blank" href="#"><i className="fa fa-file-picture-o	 icon btmspace-15 fa-font"></i></a>
                    <h6 className="heading font-x1">Gallery</h6>
                  </article>
                </li>
                <li className="one_quarter">
                  <article><a target="_blank" href="#" ><i className="fa fa-child	icon btmspace-15 fa-font"></i></a>
                    <h6 className="heading font-x1">Individual Championship</h6>
                  </article>
                </li>
                <li className="one_quarter">
                  <article><a target="_blank" href="assets/pdf/Results-Speedo-BGN-Swimming-Championship.pdf"><i className="fa fa-tasks	icon btmspace-15 fa-font"></i></a>
                    <h6 className="heading font-x1">Results</h6>
                  </article>
                </li>
              </ul>
              <div className="clear"></div>
            </div>
          </div>

          <div className="wrapper bgded overlay bgded-image">
            <article className="hoc container clear center">
              <p className="font-x1">Speedo BGN Invitational Swimming Championship 2018, New Delhi</p>
              <h2>Registrations are now closed</h2>
              {/* <h2 className="heading font-x3">Register Now & Pay Online</h2>
              <footer>
                <ul className="nospace inline pushright">
                <Link to='/delhi' target="_blank">
                <li><a className="btn" >Register & Pay Online</a></li>
                </Link>
                </ul>
              </footer> */}
              <div className="clear"></div>
            </article>
          </div>

        <div className="wrapper row3" id="mumbaisection">
        <main className="hoc container clear  padding-tb-50">
          <div className="btmspace-80 center">
            <p></p>

            <h6 className="heading">Speedo Invitational Swimming Championship 2018, Mumbai</h6>
            {/* <p className="nospace">Nisha Millets Swimming Academy is pleased to invite swimmers of your Club/School to participate in the Speedo Invitational Swimming Championship 2018, on 4th and 5th of August 2018 at the Kensington Pool, Ulsoor.     </p> */}

          </div>
          <div className="clear">
            <div className="one_half first">
              <ul className="nospace group">
                <li className="one_half first">
                  <article><a href="#"><i className="fa fa-calendar icon btmspace-30 fa-font"></i></a>
                    <h6 className="heading font-x1">Venue & Date</h6>
                    <p>Tata Power Swimming Pool, Tata Electric Officers Colony, Aziz Baug, Chembur, Mumbai - 400074</p>
                    <p className="nospace">Date: October 21st, 2018</p>
                   </article>
                 </li>
                  <li className="one_half">
                    <article><a href="#"><i className="fa fa-info icon btmspace-30 fa-font"></i></a>
                      <h6 className="heading font-x1">Details</h6>
                      <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/10/Speedo-Invitational-Swimming-Championship-Circular.pdf" target="_blank">Meet Circular</a></p>
                      <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/10/Order-of-Events-Mumbai-2018.docx">Order of Events</a></p>
                      <p>   <a href="https://goo.gl/maps/2KBWQYUvbW32" target="_blank">Find Location on Google Maps</a></p>
                      {/* <p>   <a href="" target="_blank">Complete Meet Details</a></p> */}
                      <p>   <a href="http://swimindia.in/speedo-invitational-swimming-championship-2018-mumbai-maharashtra" target="_blank" >Complete Meet Details</a></p>
                      <p>   <a href="http://swimindiain.s3.amazonaws.com/wp-content/uploads/2018/10/Offline-Entry-forms-Individual-Relay-Entry.pdf" target="_blank">Offline Entry Form</a></p>
                      <p>   <Link to='/mumbai' target="_blank">Online Registration</Link></p>

                    </article>
                  </li>
        </ul>
      </div>
              <div className="one_half"><img className="inspace-10 borderedbox" src="assets/img/landingpage/480x400-mumbai.jpg" alt="" /></div>
            </div>
            <div className="clear"></div>
  </main>
</div>


        <div className="wrapper row3">
          <div className="hoc container clear padding-t-0 padding-b-25">
            <ul className="nospace group center">
              <li className="one_quarter first">
                <article><a target="_blank" href="#"><i className="fa fa-trophy	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1 ">Medals</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a href="#"><i className="fa fa-file-picture-o	 icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Gallery</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a target="_blank" href="#"><i className="fa fa-child	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Individual Championship</h6>
                </article>
              </li>
              <li className="one_quarter">
                <article><a target="_blank" href="#"><i className="fa fa-tasks	icon btmspace-15 fa-font"></i></a>
                  <h6 className="heading font-x1">Results</h6>
                </article>
              </li>
            </ul>
            <div className="clear"></div>
          </div>
        </div>
        <div className="wrapper bgded overlay bgded-image">
          <article className="hoc container clear center">
            <p className="font-x1">Speedo Invitational Swimming Championship 2018, Mumbai</p>
            <h2>Registrations are now closed</h2>
              {/* <h2 className="heading font-x3">Register Now & Pay Online</h2>
              <footer>
                <ul className="nospace inline pushright">
                <Link to='/mumbai' target="_blank">
                <li><a className="btn" >Register & Pay Online</a></li>
                </Link>
                </ul>
              </footer> */}
            <div className="clear"></div>
          </article>
        </div>




          <div className="wrapper partnercolor">
            <article className="hoc cta clear padding-tb-20">
              <ul className="nospace inline pushright text-align-center">
                <li><img src="assets/img/landingpage/speedologo.png" className="img-width" /></li>
                <li><img src="assets/img/landingpage/Logo_Nisha_Millet_Swimming_Academy.jpg" className="img-width" /></li>
                <li><img src="assets/img/landingpage/bgn-logo.png" className="img-width" /></li>
                <li><img src="assets/img/landingpage/swimindia.png" className="img-width-swimindia" /></li>
              </ul>
            </article>
          </div>


          <div className="wrapper row5">
            <div id="copyright" className="hoc clear">
              <p className="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="http://www.swimindia.in" target="_blank">SwimIndia</a></p>
            </div>
          </div>
         <a id="backtotop" href="#top"><i className="fa fa-chevron-up"></i></a>
         </div>
        )
        }
      }
      export default App;
