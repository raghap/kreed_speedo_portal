import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import ClubSelect from '../components/fields/ClubSelect.js';
import CheckBox from '../components/fields/CheckBox.js';
import Radio from '../components/fields/Radio.js'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name:"Speedo Invitational Swimming Championship 2018, Bangalore",
      Kreed_ID:"",
      full_name: "",
      second_name: "",
      date_of_birth: "",
      gender: "",
      mobile_number: "",
      email: "",
      parent_name: "",
      parent_mobile_number: "",
      city: "",
      club_name: "",
      ksaid_status: "No",
      clubList: ["Abba Sports Club","Aqua Swimming Centre","Army Public School","Association of Integrated Mysore Swimmers AIMS",
      "Baldwin Boy’S High School","Baldwin Girl'S High School","Baldwin Girl'S High School","Bangalore Club",
      "Bangalore International School","Bangalore Swimming Research Centre","Basava Rajeshwari Public School",
      "Basavanagudi Aquatic Centre","Bishop Cotton'S Boys High School","Carmel School","Christ Academy",
      "Chrysalis High","Concorde International","Davanagere Swimming Aquatics","Dolphin Aquatics",
      "DPS","Edify School","Ekalavya","Frank Anthony Public School","G.B.M.M. High School","Glitzy Swimming Centre",
      "Global City International School","Global Swim Centre","Glorious Aquatics","Golden Fins Sports Club",
      "Greenwood High International","Gulbarga Aqua Association","Gurukul Sport","Indus International School",
      "Inventure Academy","Jai Hind Swimming Club","JSS Public School","Kempegowda Swimming Pool",
      "Kendriya Vidyalaya Malleshwaram","Lakeshore Swimming Club","LR English School","LSH Club","M K Sports Club",
      "Mahila Seva Samaja","Mallya Adithi International School","Mangala Swimming Club","Mangalore Aquatic Club",
      "Matsya Aquatics","MG Sports","Michael Phelps Swimming School","Mysore Dist Swimming Association",
      "Naresh Swimming Academy","National Publc School","National Public School Kengeri","Neev Academy",
      "Nettakallappa Aquatic Center","Nisha Millet Swimming Academy","NMSA Marlins","Orchids International School",
      "Passionate Sports Academy","PET Aquatic Centre","Pooja Aquatic Centre","Poornaprajna Education Centre",
      "Presidency School","Pupil Tree School","Puttur Aquatic Club","Rashtrotthana Vidya Kendra",
      "Ray Centre","Ryan International School","Shishu Griha School","Shivamogga Sports Complex",
      "Shri Kumarans High School","Shri Manjunatheshwara Central School","SIMS Aquatics",
      "SIMS Aquatics","Sophia High School","Soundarya Aquatic Centre","Spark Aquatics",
      "Splash Aquatics","Spoorthi Aquatic Gulbarga","Sprint Swimming Academy","Sri Sharada Vidyanikethan",
      "Srujan Aquatics","Star Academy","Star Aquatic Club","Star Fish Academy","Sudarshan Vidya Mandir",
      "SVM International School","SwimIndia","SwimLife Swimming Academy","Swimmers Club Belgaum",
      "The Brigade School","The Foundation School","TISB","Vibgyor High","Vidya Sanskar International School",
      "Vidyanikethan","Vidyashilp Academy","Vijayanagar Aquatic Centre","Vishwabharathi Public School",
      "Young Challengers Swim CLub","Zee Swim Academy","Others"],
      swimGroup: "",
      eventList: [],
      athleteInfo: "",
      amount_to_pay: "0",
      events:"",
      selectedEvents:[],
      selectedEventTimings:[],
      agree_TC: "",
      selectedClubValue:"",
      isLoading: false,
      errorAlert: false,
      // athleteLoadingError: false,
      athleteObj:{},
      newKreedID:"",
      enrollmentID:"",
      // Navigation for different pages
      register: "Yes",
      thankyou: "No",
      paymentSuccess: "No"
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.getGroup = this.getGroup.bind(this);
    this.getAthleteDetails = this.getAthleteDetails.bind(this);
    this.getEventList = this.getEventList.bind(this);
    this.resetdata = this.resetdata.bind(this);
    this.submitRegistration = this.submitRegistration.bind(this);
    this.capturePayment = this.capturePayment.bind(this);
    this.getInputFields = this.getInputFields.bind(this);
    this.displayRegisteredEvents = this.displayRegisteredEvents.bind(this);
    this.payOnline = this.payOnline.bind(this);
  }
  resetdata() {
    var newState = {};
      newState.full_name = "";
      newState.second_name = "";
      newState.date_of_birth = "",
      newState.gender = "",
      newState.mobile_number = "",
      newState.email = "",
      newState.parent_name = "",
      newState.parent_mobile_number = "",
      newState.events = "",
      newState.city = "",
      newState.club_name = "",
      newState.selectedEvents = [],
      newState.selectedEventTimings = [],
      newState.ksa_id = "",
      newState.eventList = [],
      newState.selectedClubValue = ""
    this.setState(newState);

  }
  getEventList() {
    var events = [];

    this.state.amount_to_pay = "600";

    if (this.state.swimGroup == "Masters Group A" || this.state.swimGroup == "Masters Group B" || 
          this.state.swimGroup == "Masters Group C" || this.state.swimGroup == "Masters Group D") 
    {
      events = ["50m FS", "50m BK", "50m BR"];
    } 
    else if (this.state.swimGroup == "Group I" || this.state.swimGroup == "Group II" || 
              this.state.swimGroup == "Group III") 
    {
      events = ["50m FS", "100m FS", "100m BK", "100m BR", "100FLY", "200m IM"];
    } 
    else if (this.state.swimGroup == "Group IV") 
    {
      events = ["50m FS", "100m FS", "50m BK", "50m BR", "50FLY", "200m IM"];
    } 
    else if (this.state.swimGroup == "Group V") 
    {
      events = ["50m FS", "50m BK", "50m BR", "50FLY"];
    } 
    else if (this.state.swimGroup == "Group VI") 
    {
      events = ["Kick Board Race"];
      this.state.amount_to_pay = "200";
    }
    
    this.setState({ eventList: events })
  }
  // startDOB() {
  //   // return new Date(new Date().setFullYear(new Date().getFullYear() - 6));
  //   var year = (new Date().getFullYear() - 6);
  //   var date = new Date(year, 11, 31);
  //   return date;
  // }
  getAthleteDetails(e) {
    e.preventDefault();

    this.setState({ isLoading: true });
    var getAthleteDetails=[];
    var ksaID = this.state.ksa_id;
    fetch("https://us-central1-kreed-of-sports.cloudfunctions.net/searchByKSAID?ksaID=" + ksaID)
      .then(res => res.json())
      .then(
        (result) => {
          var myDate =  moment(result.date_of_birth).format('DD-MM-YYYY');
          // var myDate = moment(result.date_of_birth).toDate();
          this.setState({ full_name: result.full_name, gender: result.gender, date_of_birth: myDate,
             mobile_number: result.mobile_number, email: result.email, parent_name: result.father_name,
             swimGroup:result.swimGroup,city:result.city, club_name:result.club_name,Kreed_ID:result.Kreed_ID , isLoading: false});
            this.getEventList();
          },
      
        (error) => {
          this.state.errorStatement = "Please Enter a correct KSA ID"
          this.setState({isLoading: false , errorAlert: true});
          // this.setState({
          //   isLoaded: true,
          //   error
          // });
        }
      )
      
     
  }
  // to get group
  getGroup(dob) {
    var timestamp = Date.parse(dob);

    if (isNaN(timestamp))
      return "Open";

    var current = new Date();
    var dob_dt = new Date(dob);
    var today = current.getFullYear() - dob_dt.getFullYear();

    if (today > 54) {
      return "Masters Group D";
    } else if (today == 18 || today == 19 || today == 20 || today == 21 || today == 22 || today == 23 || today == 24) {
      return "Ages 18 to 24 are not permitted.";
    } else if (today == 17 || today == 16 || today == 15) {
      return "Group I";
    } else if (today == 14 || today == 13) {
      return "Group II";
    } else if (today == 12 || today == 11) {
      return "Group III";
    } else if (today == 10 || today == 9) {
      return "Group IV";
    } else if (today == 8 || today == 7) {
      return "Group V";
    } else if (today == 6) {
      return "Group VI";
    } else if (today == 25 || today == 26 || today == 27 || today == 28 || today == 29 || today == 30 || today == 31 || today == 32 || today == 33 || today == 34) {
      return "Masters Group A";
    } else if (today == 35 || today == 36 || today == 37 || today == 38 || today == 39 || today == 40 || today == 41 || today == 42 || today == 43 || today == 44) {
      return "Masters Group B";
    } else if (today == 45 || today == 46 || today == 47 || today == 48 || today == 49 || today == 50 || today == 51 || today == 52 || today == 53 || today == 54) {
      return "Masters Group C";
    } else {
      return "Kids below 6 years of age are not permitted."
    }
  }
  componentDidMount() {
    // this.findFirestorePath();
  }
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
    if (name == "parent_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
   

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }

    if(name.startsWith("eventTiming")){
      var index = name.split("_")[1];
      var timings = this.state.selectedEventTimings;
      timings[index] = value;
      this.setState({selectedEventTimings: timings});
      return;
    }

    this.setState({
      [name]: value
    });
    // alert(this.state.events);
  }
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/assoc/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
        // if (!this.state.editedFields.includes(key))
        //   this.state.editedFields.push(key);
        if (key == "ksaid_status")
          this.resetdata();

        // if (key == "photo")
        //   this.state.imageLoading = false;
        if (key == "date_of_birth") {
          this.state.selectedEvents = "";
          this.state.events = "";
          this.state.swimGroup = this.getGroup(value);
          this.getEventList();
          // this.state.dobEntered = true;
        }
        if(key == "events"){
          //alert("VALUE:" +value);
          var evts = value.slice(1).split(",");    
          this.setState({ selectedEvents: evts });
        }
        // alert(this.state.selectedEvents)
        

        this.setState({ [key]: value });
      }

    }
  }
  getInputFields(){
    var i;
    var items = [];
    var event_name = [];
    var selectedEvtTime = []
    for (i = 0; i < this.state.selectedEvents.length; i++) {
      items.push(
      <div className="row">
      <div className="col-sm-2">
        <h4 className="eventname-styling">
        {this.state.selectedEvents[i]}
        </h4>
      </div>
      <div className="col-sm-4">
       <input type="text" placeholder="00:00.00" name={ "eventTiming_" + i} aria-required="true" value={this.state.selectedEventTimings[i]} onChange={this.handleInputChange} className="form-control" />
      </div>
    </div> 
      );
    }
    return items;
  }
  displayRegisteredEvents(){
    var i;
    var items = [];

    for (i = 0; i <  this.state.selectedEvents.length; i++) {
      items.push(<li>{this.state.selectedEvents[i]}</li>);
    }
    return items;
  }

  getDataObject()
  {
      var dObj = {};

      if(typeof this.state.ksa_id == 'string' && this.state.ksa_id.length > 0)
      {
        dObj.KSA_ID = this.state.ksa_id;
        dObj.Kreed_ID = this.state.Kreed_ID;
      }
      else
      {
        dObj.full_name = this.state.full_name;
        dObj.date_of_birth = this.state.date_of_birth;
        dObj.gender = this.state.gender;
        
        dObj.parent_name = this.state.parent_name;
        dObj.swimGroup = this.state.swimGroup;
        dObj.city = this.state.city;
        dObj.country = "India";
      }

      //common fields
      dObj.mobile_number = this.state.mobile_number;
      dObj.email = this.state.email;
      dObj.parent_mobile_number = this.state.parent_mobile_number;
      dObj.club_name = this.state.club_name;
      dObj.amount = this.state.amount_to_pay;

      //event regn fields - always carried.
      dObj.brand_name = "speedo";
      dObj.meet_name = this.state.meet_name;
      dObj.selected_events = this.state.selectedEvents.slice();
      dObj.selected_event_timings = this.state.selectedEventTimings.slice();

    return dObj;

  }

  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.state.newKreedID, enrollment_id: this.state.enrollmentID, payment_id: payResp.razorpay_payment_id, brand_name: "speedo", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true ,thankyou: "No",paymentSuccess: "Yes"  });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }

  payOnline(){
    var options = {
      "key": "rzp_live_BqIL7YhCz70yHf",
      "amount": this.state.amount_to_pay * 100 , // 2000 paise = INR 20
      "name": "SwimIndia",
      "description": "Speedo Invitational Swimming Championship 2018, Bangalore",
      // "image": "/your_logo.png",
      "handler": this.capturePayment,
      "prefill": {
          "contact": this.state.mobile_number,
          "email": this.state.email,
      },
      "notes": {
          "address": this.state.city
      },
      "theme": {
          "color": "#528FF0"
      }
    };
    var rzp = new window.Razorpay(options);
    rzp.open();

  }
  submitRegistration(e)
  {
      e.preventDefault();

      if(this.state.date_of_birth == "" ){
        this.state.errorStatement = "Select Your Date Of Birth";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length < 1 ){
        this.state.errorStatement = "Select atleast one event";
        this.setState({errorAlert: true})
        return;
      }
      if(this.state.selectedEvents.length >= 4 ){
        this.state.errorStatement = "You can select a maximum of 3 events";
        this.setState({errorAlert: true})
        return;
      }
      if(this.state.mobile_number.length < 10 ){
        this.state.errorStatement = "Please ensure you have entered a 10 digit mobile number";
        this.setState({errorAlert: true})
        return;
      }
      if(this.state.gender === "" ){
        this.state.errorStatement = "Please ensure you have selected a gender";
        this.setState({errorAlert: true})
        return;
      }
      if(this.state.club_name == ""){
        this.state.errorStatement = "Select Your Club";
        this.setState({errorAlert: true})
        return;
      }
      this.setState({ isLoading: true });
      axios({
        method: 'post',
        headers:  {
                      'Content-Type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                  },
        url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitMeetRegistration',
        data: this.getDataObject()
      })
      .then(response => {
        // alert("KreedId" + response.data.kreedID + ", Enrolment Id: " + response.data.enrollmentID);
        this.state.newKreedID = response.data.kreedID;
        this.state.enrollmentID = response.data.enrollmentID;
        // Navigating to thankyou PAGE
        this.setState({register: "No" , thankyou: "Yes",isLoading: false });
      })
      .catch(error => {
        this.state.errorStatement = "Error completing registration";
        this.setState({ isLoading: false, errorAlert: true })
        // console.log("Error submitting athlete details: ", error);
      })
 }


//Container Render Method
render() {
  return (
    <div className="content">
       <img alt="registration are closed" src='assets/img/Registrations_are_closed.png' />
    </div>
  )
}
  render_notused() {
    return (
      
      <div className="content">
      {/* SWEET ALERTS */}
      <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
                <img className="banner-img-styling" src='assets/img/wallpaper.jpg' />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
                <img className="wrapper-img-styling" src='assets/img/wrapper.jpg' />
                <h2>{this.state.meet_name}</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Main Form */}
          {this.state.register == "Yes" && <div> <div className="row whitebg border padding-20">
            {/* Address */}
             <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <p className="margin-0px"><b>Venue: </b>Kensington Pool, Ulsor, Bangalore</p>
                <p className="margin-0px"><a target="_blank" className="linkStyling text-color" href="https://www.google.co.in/maps/place/Kensington+Swimming+Pool/@12.9840455,77.6198486,17z/data=!4m5!3m4!1s0x3bae16919fef89ed:0x2a81cabee0030d69!8m2!3d12.9840455!4d77.6220373?shorturl=1"><b>Location Map</b></a></p>
                <p className="margin-0px"><b>Dates: </b>August 4th 2018, Saturday – Group 4 and Group 5 <br />
                  August 5th 2018, Sunday – Group 1, Group 2 and Group 3, MASTERS</p>
                <p className="margin-0px"><b>Warm-up: </b>7am - 7:45am</p>
                <p className="margin-0px"><b>Competition Start Time: </b>8am</p>
                Visit <a target="_blank" className="linkStyling" href="http://swimindia.in/2nd-speedo-invitational-swimming-championship-2018-bangalore">SwimIndia</a> to know more about the meet.
              </div>
            </div>
            {/* Address */}
            {/* Form */}
            <form onSubmit={this.submitRegistration}>
            <div className="col-sm-12 common-padding">
              {/* Do You have a KSA ID */}
              <div className="col-sm-12">
                <label className="control-label label-color">Do you have KSA ID?</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <Radio theKey="ksaid_status" updateAction={this.updateAction} value={this.state.ksaid_status} options={["Yes", "No"]} />
                  </div>
                </div>
              </div>
              <div className="col-sm-12">
              <p className="margin-0px">(For faster registration, please enter your KSA ID)</p>
              </div>
              {/* Do You have a KSA ID   */}
              {/* KSA ID */}
              {this.state.ksaid_status == "Yes" && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color">KSA ID :</label>
                </div>
                <div className="col-sm-4 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <input type="text" name="ksa_id" aria-required="true" value={this.state.ksa_id} onChange={this.handleInputChange} className="form-control" />
                    </div>
                  </div>
                </div>
                <div className="col-sm-1 padding-0 margin-padding-0 margin-20">
                <button onClick={this.getAthleteDetails} className="btn btn-size font-size btn-margin-padding" >Go</button>
                  {/* <a onClick={this.getAthleteDetails} className="Va-middle"><i className="material-icons">arrow_forward</i></a> */}
                </div>
              </div>}
              {/*KSA ID*/}
              {/* If KSA id is not there */}
              <div className="col-sm-12">
                <label className="control-label label-color">Swimmer Name <span className="color-red"> *</span></label>
              </div>
              {/* First Name */}
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  {this.state.ksaid_status == "Yes" &&  <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" readOnly/>}
                  {this.state.ksaid_status == "No" &&  <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />}
                  </div>
                </div>
              </div>
              {/* First Name */}
              {/* Date of birth */}
              <div className="col-sm-12">
                <label className="control-label label-color">Date Of Birth <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  {this.state.ksaid_status == "Yes" && <input type="text" required="true" name="date_of_birth" aria-required="true" value={this.state.date_of_birth} onChange={this.handleInputChange} className="form-control" readOnly/>}
                  {this.state.ksaid_status == "Yes" && <p>If shown DOB is incorrect.Please contact<a href="mailto:contact@swimindia.in" > SwimIndia</a></p>}
                  {this.state.ksaid_status == "No" &&  <KreedDatePicker theKey="date_of_birth" required="true" value={this.state.date_of_birth} updateAction={this.updateAction} />}
                  </div>
                </div>
              </div>
              {/* Date of birth */}
              {/* Gender */}
              <div className="col-sm-12">
                <label className="control-label label-color">Gender <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  {this.state.ksaid_status == "Yes" && <Radio theKey="gender"  required="true" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]}  disabled={true}  />}
                  {this.state.ksaid_status == "No" && <Radio theKey="gender"  required="true" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />}
                  </div>
                </div>
              </div>
              {/* Gender */}
              {/* Phone */}
              <div className="col-sm-12">
                <label className="control-label label-color">Phone <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Phone */}
              {/* Email */}
              <div className="col-sm-12">
                <label className="control-label label-color">Email <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Email */}
              {/* Parents name */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Name <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  {this.state.ksaid_status == "Yes" && <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" readOnly/>}
                  {this.state.ksaid_status == "No" && <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" />} 
                  </div>
                </div>
              </div>
              {/* Parents name */}
              {/* Parent contact number */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Contact Number</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="parent_mobile_number" value={this.state.parent_mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parent contact number */}
              {/* city */}
              <div className="col-sm-12">
                <label className="control-label label-color">City</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                  {this.state.ksaid_status == "Yes" && <input type="text" name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" readOnly/>}
                  {this.state.ksaid_status == "No" && <input type="text"  name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />}
                  </div>
                </div>
              </div>
              {/* city */}
              {/* Club/School */}
              <div className="col-sm-12">
                <label className="control-label label-color">Club/School<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <ClubSelect theKey="club_name" updateAction={this.updateAction} value={this.state.club_name} options={this.state.clubList}/>
                  </div>
                </div>
              </div>
              {/* Club/School */}
              {/* If KSA id is not there */}
              {/* Group and Gender Details*/}
              {this.state.swimGroup != "" && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> {this.state.swimGroup}</label>
                </div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> Select Events</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="events" updateAction={this.updateAction} value={this.state.events} options={this.state.eventList} />
                    </div>
                  </div>
                </div>
              </div>}
              {/* Group and Gender Details*/}
              {/* Timings for each events */}
              {this.state.selectedEvents.length >= 4 && <div>
                <div className="col-sm-12">
                  <label className="control-label color-red">You can only choose upto 3 choice(s).</label>
                </div>
                </div>}
              {(this.state.selectedEvents.length > 0 && this.state.selectedEvents.length < 4) && <div>
               {(this.state.swimGroup != "Group V" && this.state.swimGroup != "Group VI") && <div> <div className="col-sm-12">
                  <label className="control-label label-color">Enter Timing for Selected Events </label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      {this.getInputFields()}
                    </div>
                  </div>
                </div>
                </div>}
                </div>}
              {/* Timings for each events */}
              {/* Payment */}
              <div className="col-sm-12">
                <label className="control-label label-color">Registration Fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-4 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.amount_to_pay} </p>
                  </div>
                </div>
              </div>
              {/* Payment */}
              {/* Terms and Conditions */}
              <div className="col-sm-12 ">
                <label className="control-label label-color">Terms and Conditions <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-10">
                <div className="border-height">
                <ol className="padding-left-25">
                  <label className="control-label label-color margin-tb-20">Eligibility Criteria </label>
                  <li>Swimmers will not be allowed to take part if he/she is a medalist in an individual event at any State, Zonal, National and International meet conducted by FINA, Karnataka Swimming Association (KSA), Swimming Federation of India (SFI), School Federation/Department of Youth Services & Sports (DYSS), ICSE and CBSE boards till the date of the competition.</li>
                  <li>Swimmers who have won medals in the relay events at the State, Zonal, National & International championships will be allowed to take part in this meet.  </li>
                  <li>The competition will be held according to the FINA Rules.</li>
                  <li>Entry Fees of Rs. 600.00/-for events shall be paid online at the below link.</li>
                  <li>Details of the Names of the Competitors, Date of Birth, Age Group and details of the events along with their timings is mandatory while filing nominations.</li>
                  <li>The Age Certificate duly signed by the Head of the Educational Institution “OR” First Class Magistrate “OR” Municipal Corporation must be submitted along with the nomination entries to the organisers.</li>
                  <li>Swimmers will be eligible to take part in maximum of the 3 events.</li>
                  <li>Events will be held only on “TIME FINALS” basis.</li>
                  <li>Swimming events will be conducted with “ONE START” rule.</li>
                  <li>Decision of the Referee is Final and the organisers shall not entertain any protests.</li>
                  <li>Last date for the submission of the entry is 1st August, 2018. </li>
                  <li>Strictly no spot entries. All the participants will receive participation certificate. Medals and certificate will be honoured to all the winners. Swimmers with highest points in each group will receive Individual Championship and kits from Speedo. Top 2 teams with highest points will receive Overall and runners-up trophies.</li>
                  <li>For queries please contact: Nisha Millet: +91 9880196782. Sharath: +91 8904482502</li>
                  <li>For registration related enquiries, call Sumanth +91 91084 56704</li>
                </ol>
                <ul>
                <label className="control-label label-color margin-tb-20">Note: </label>
                  <li>Group I and Group II will be a combined relay event in which Group I and Group II swimmers can combine to form a single relay team in any ratio combination with regards to the age group</li>  
                  <li>Each club can send only one relay team for an event</li>
                </ul>
                </div>
              </div>
              <div className="col-sm-12 margin-tb-20  ">
                <input className="margin-5" type="checkbox" required="true" name="agree_TC" aria-required="true" value={this.state.agree_TC} onChange={this.handleInputChange}/>By clicking here , I agree to the terms & conditions 
              </div>
              {/* Terms and Conditions */}
            </div>
            <div className="row margin-top-25px">
                  <div className="col-sm-12 text-align-center">
                    <button className="btn btn-size font-size" value="Save">Submit</button>
                  </div>
                </div>
          </form>
          </div>
            {/* Form */}
          </div>}
          {/* Main Form */}
          {/* thankyou Page */}
          {this.state.thankyou == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="row">
                <div className="col-sm-12 details-padding">
                  <p>Dear <b>{this.state.full_name} ,</b></p>
                  <p>Thank you for registering to <b> {this.state.meet_name}</b></p>
                  <h4> <b>Registration Details: </b></h4>
                  {/* Swimmser name */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Swimmer Name : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.full_name}</p>
                    </div>
                  </div>
                  {/* Swimmer name */}
                  {/* Group */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Group : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.swimGroup}</p>
                    </div>
                  </div>
                  {/* Group */}
                  {/* Events Selected */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Events Selected : </label>
                    </div>
                    <div className="col-sm-10">
                      <ol className="padding-l-15">
                          {this.displayRegisteredEvents()}
                      </ol>
                    </div>
                  </div>
                  {/* Events Selected */}
                  {/* Amount to be paid */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Amount To Be Paid : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> Rs. {this.state.amount_to_pay}</p>
                    </div>
                  </div>
                  <p className="color-red">Your registration will be confirmed only after payment is completed.</p>
                  {/* Amount to be paid */}
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                     <button onClick={this.payOnline} className="btn btn-size font-size" value="Save">Pay Online</button>
                    </div>
                  </div>
                </div>
              </div>
           </div>
          </div>}
          {/* Thankyou Page */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p>
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>{this.state.meet_name},</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
      </div>
    )
  }
}
export default App;
