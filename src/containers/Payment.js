import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import SweetAlert from 'react-bootstrap-sweetalert';
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

const HASH_SEED = "arTBk2DfvbZ0uCZD9Vij6d7Y2g33CvdfoGI6n3yEzK05yPTuA90hL6z0ddwokCDE";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name:"",
      img_src_wallpaper:"",
      img_src_wrapper:"",
      isLoading: false,
      errorAlert: false,
      paymentSuccess: "No",
      rzpKey: "",
    }
    this.capturePayment = this.capturePayment.bind(this);
    // this.launchRazorPay = this.launchRazorPay.bind(this);
  }

  hash(str) {
    var hash = 5381, i = str.length;
  
      while(i) {
        hash = (hash * 33) ^ str.charCodeAt(--i);
      }
      return hash >>> 0;
  }
  
  
 
  componentDidMount() {

    if(typeof(this.props.location.query.chk) == "string") {
      var chkstr = this.hash(HASH_SEED +  this.props.location.query.krid + 
                 this.props.location.query.amount + this.props.location.query.enrollment_id +
                 this.props.location.query.email + this.props.location.query.contact);

      if(chkstr != this.props.location.query.chk){
        this.state.errorStatement = "Invalid Payment URL";
        this.setState({ errorAlert: true });
        return;
      }
    }

    fetch("https://us-central1-kreed-of-sports.cloudfunctions.net/searchByEnrollmentID?brand=speedo&enrollment_id=" + this.props.location.query.enrollment_id )
    .then(res => res.json())
    .then(
      (result) => {
        var strMeetName = result.meet_name.replace(/[^A-Z0-9]+/ig, "_");
        var img_src_wallpaper = 'assets/img/' + strMeetName + '_wallpaper.png';
        var img_src_wrapper = 'assets/img/' + strMeetName + '_wrapper.png';
        var rzpKey = strMeetName.toLowerCase().includes("bangalore") ? "rzp_live_BqIL7YhCz70yHf" : "rzp_live_SKW6ZcvymAaWBJ";
        this.setState({ rzpKey: rzpKey, meet_name: result.meet_name, img_src_wallpaper: img_src_wallpaper ,img_src_wrapper: img_src_wrapper})
     
        var options = {
          "key": this.state.rzpKey,
          "amount": this.props.location.query.amount * 100 , // 2000 paise = INR 20
          "name": "SwimIndia",
          "description": this.state.meet_name,
          "handler": this.capturePayment,
          "prefill": {
              "contact": this.props.location.query.contact,
              "email": this.props.location.query.email,
          },
          "notes": {
              "address": "Bangalore"
          },
          "theme": {
              "color": "#528FF0"
          }
        };
    
        var rzp = new window.Razorpay(options);
        rzp.open();

      },
    
      (error) => {
        this.state.errorStatement = "Error in connecting to server"
        this.setState({ errorAlert: true});
        // console.log("Error"+error);
      }
    );
 
      


  }


  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.props.location.query.krid, enrollment_id: this.props.location.query.enrollment_id,
                 payment_id: payResp.razorpay_payment_id, brand_name: "speedo", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true, paymentSuccess: "Yes" });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }
//Container Render Method
  render() {
    return (
      
      <div className="content">
       {/* SWEET ALERTS */}
       <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
                <img className="banner-img-styling" src={this.state.img_src_wallpaper} />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
                <img className="wrapper-img-styling" src={this.state.img_src_wrapper} />
                <h2>{this.state.meet_name}</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            {/* <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p> */}
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>{this.state.meet_name},</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
      </div>
    )
  }
}
export default App;
