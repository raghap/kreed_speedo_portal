import React, { Component } from 'react';
import { render } from 'react-dom'

import Master from './containers/Master'
import App from './containers/App'
import Bangalore from './containers/Bangalore'
import Payment from './containers/Payment'
import Delhi from './containers/Delhi'
import Mumbai from './containers/Mumbai'

import Config from   './config/app';

import { Router, Route,hashHistory,IndexRoute } from 'react-router'

class Admin extends Component {

  //Prints the dynamic routes that we need for menu of type fireadmin
  // getFireAdminRoutes(item){
  //   if(item.link=="fireadmin"){
  //     return (<Route path={"/fireadmin/"+item.path} component={Fireadmin}/>)
  //   }else{

  //   }
  // }

  // //Prints the dynamic routes that we need for menu of type fireadmin
  // getFireAdminSubRoutes(item){
  //   if(item.link=="fireadmin"){
  //     return (<Route path={"/fireadmin/"+item.path+"/:sub"} component={Fireadmin}/>)
  //   }else{

  //   }
  // }

  //Prints the Routes
  /*
  {Config.adminConfig.menu.map(this.getFireAdminRoutes)}
  {Config.adminConfig.menu.map(this.getFireAdminSubRoutes)}
  */
  render() {
    return (
      
        <Router history={hashHistory}>
        
          <IndexRoute component={App}></IndexRoute>

          <Route path="/" component={App}/>

          <Route path="/bangalore" component={App}/>
          <Route path="/delhi" component={App}/>
          <Route path="/mumbai" component={Mumbai}/>

          {/* <Route path="/test_mumbai" component={Mumbai}/> */}
          <Route path="/payment" component={Payment}/>

      </Router>
     
    );
  }

}

export default Admin;
