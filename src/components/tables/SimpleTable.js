import React, { Component, PropTypes } from 'react'
import Config from '../../config/app';
import Common from '../../common.js'
import { Link, IndexLink, withRouter } from 'react-router'

const ConditionalWrap = ({ condition, wrap, children }) => condition ? wrap(children) : children;
const ConditionalDisplay = ({ condition, children }) => condition ? children : <div></div>;

class SimpleTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      headers: this.props.headers,
      data: this.props.data,
      filter: ""
    };
    this.createTableRow = this.createTableRow.bind(this);
    this.deleteAction = this.deleteAction.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.createUpwardButton = this.createUpwardButton.bind(this);
    this.createDownwardButton = this.createDownwardButton.bind(this);
    this.moveUp=this.moveUp.bind(this);
    this.moveDown = this.moveDown.bind(this);
  }

  /**
  * componentDidMount event of React, fires when component is mounted and ready to display
  * Start connection to firebase
  */
  componentDidMount() {
    console.log(this.props);

    if (this.props.headers && this.props.headers.length > 0) {
      //We already know who are our headers,preset
    } else {
      //Loop throught all items ( in data ) to find our headers
      var headers = []
      var headersCounter = {};
      for (var i = 0; i < this.props.data.length; i++) {

        //The type of our array items
        var parrentType = Common.getClass(this.props.data);
        var type = Common.getClass(this.props.data[i]);
        console.log("parrentType type is " + parrentType);
        console.log("Inside type is " + type);

        //OBJECTS INSIDE
        if (type == "Object") {

          //In CASE we have OBJECT as array items
          for (var key in this.props.data[i]) {
            // skip loop if the property is from prototype
            if (!this.props.data[i].hasOwnProperty(key)) continue;

            var obj = this.props.data[i][key];
            var objType = Common.getClass(obj);

            //Consider onyl String, Bool, Number
            if ((objType === "String" || objType === "Boolean" || objType === "Number") && key != "uidOfFirebase") {
              if (headersCounter[key]) {
                headersCounter[key]++
              } else {
                headersCounter[key] = 1;
              }
            }
          }
        }

        //STRING INSIDE
        else if (type == "String") {
          headers = ["Value"];
          headersCounter["Value"] = 1;
          break;
        }
      }

      console.log("headersCounter")
      console.log(headersCounter)
      //END looking for headers

      var numHeadersCounter = 0;
      for (var key in headersCounter) {
        numHeadersCounter++;
      }

      console.log("numHeadersCounter " + numHeadersCounter);

      //ARRAYS INSIDE
      if (numHeadersCounter == 0) {
        console.log("Make it ArtificialArray");
        headers = ["Items"];
        headersCounter["Items"] = 1;
        type = "ArtificialArray"; //Artificial
      }

      //Now we have the headers, with their number of occurences
      //Convert object to array
      var headersCounterAsArray = [];
      for (var key in headersCounter) {
        headersCounterAsArray.push({ key: key, counter: headersCounter[key] })
      }


      headersCounterAsArray.sort(function (b, a) {
        return parseFloat(a.counter) - parseFloat(b.counter);
      });

      console.log("headersCounterAsArray length " + headersCounterAsArray.length)
      console.log(headersCounterAsArray)


      //Pick headers based on their number of appereances 2
      headers = [];
      for (var k = 0; k < headersCounterAsArray.length && k < Config.adminConfig.maxNumberOfTableHeaders; k++) {
        console.log("Is it ok " + (k < headersCounterAsArray.length && k < Config.adminConfig.maxNumberOfTableHeaders))
        headers.push(headersCounterAsArray[k].key)
      }

      //Update the state
      console.log(headers);
      this.setState({ headers: headers, type: type })
    }


  }

  deleteAction(index, theLink) {
    console.log(index);
    if (this.props.isFirestoreSubArray) {
      this.props.deleteFieldAction(index, true, theLink);
    } else {
      this.props.deleteFieldAction(index, true);
    }

  }

  createEditButton(theLink) {
    if (this.props.isFirestoreSubArray) {
      return (<a onClick={() => { this.props.showSubItems(theLink) }}><span className="btn btn-success btn-icon margin-lr-5 edit"><i className="material-icons">mode_edit</i></span></a>);
    } else {
      return (<Link data-toggle="tooltip" title="Edit" to={theLink}>
        <span className="btn btn-success btn-icon margin-lr-5 edit"><i className="mdi material-icons">mode_edit</i></span>
      </Link>)
    }
  }
 
  moveUp(i) {
    var event1 = this.props.data[i].meet_id + "/sessions/" + 
                  this.props.data[i].session_id + "/events/" +
                  this.props.data[i].event_id;

    var event2 = this.props.data[i-1].meet_id + "/sessions/" + 
                  this.props.data[i-1].session_id + "/events/" +
                  this.props.data[i-1].event_id;             

    this.props.swapSequence(event1, event2);
  }

  createUpwardButton(index) {
    return (
      <a data-toggle="tooltip" title="Move Up" onClick={() => { this.moveUp(index) }} >
        <span className="btn btn-warning btn-icon margin-lr-5 delete"><i className="material-icons">arrow_upward</i></span></a>
    )
  }

  moveDown(i) {

    var event1 = this.props.data[i].meet_id + "/sessions/" + 
                  this.props.data[i].session_id + "/events/" +
                  this.props.data[i].event_id;

    var event2 = this.props.data[i+1].meet_id + "/sessions/" + 
                  this.props.data[i+1].session_id + "/events/" +
                  this.props.data[i+1].event_id;             

    this.props.swapSequence(event1, event2);
  }

  createDownwardButton(index) {
    return (
      <a data-toggle="tooltip" title="Move Down" onClick={() => { this.moveDown(index) }}>
        <span className="btn btn-warning btn-icon margin-lr-5 delete"><i className="material-icons">arrow_downward</i></span></a>
    )
  }


  createTableRow(item, index) {
    var theLink = this.props.routerPath.replace(":sub", "") + this.props.sub;
    if (this.props.isFirestoreSubArray) {
      if (this.props.fromObjectInArray) {
        theLink += Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
      } else {
        if (this.props.isJustArray) {
          theLink += Config.adminConfig.urlSeparatorFirestoreSubArray + index;
        } else {
          theLink += Config.adminConfig.urlSeparatorFirestoreSubArray + this.props.name + Config.adminConfig.urlSeparatorFirestoreSubArray + index;
        }
      }
    } else {
      if (this.props.fromObjectInArray) {
        theLink += Config.adminConfig.urlSeparator + item.uidOfFirebase;
      } else {
        if (this.props.isJustArray) {
          theLink += Config.adminConfig.urlSeparator + index;
        } else {
          theLink += Config.adminConfig.urlSeparator + this.props.name + Config.adminConfig.urlSeparator + index;
        }
      }
    }






    //theLink="/fireadmin/Categories/items+0+"+this.props.name+"+"+index;
    //theLink="/"
    //console.log(theLink);
    
    return (<tr>
      <td> { index+1 } </td> 
      {this.state.headers ? this.state.headers.map((key, subindex) => {
        if (Config.adminConfig.fieldsTypes.photo.indexOf(key) > -1) {
          //This is photo
          return (<td><div className="tableImageDiv" ><Link to={theLink}><img className="tableImage" src={item[key]} width={"200px"} /></Link></div></td>)
        } else {
          //Normal value
          //But can be string
          if (this.state.type == "String") {
            return subindex == 0 ? (<td><Link to={theLink}>{item}</Link></td>) : (<td>{item}</td>)
          } if (this.state.type == "ArtificialArray") {
            if (Config.adminConfig.showItemIDs) {
              return subindex == 0 ? (<td><Link to={theLink}>{this.props.data[subindex].uidOfFirebase}</Link></td>) : (<td>{this.props.data[subindex].uidOfFirebase}</td>)
            } else {
              return subindex == 0 ? (<td><Link to={theLink}>{"Item " + (index + 1)}</Link></td>) : (<td>{"Item " + (index + 1)}</td>)
            }

          } else {
            return subindex == 0 ? (<td><Link to={theLink}>{item[key]}</Link></td>) : (<td>{item[key]}</td>)
          }

        }

      }) : ""}
      <td className="td-actions text-right">
        {index !== 0 ? this.createUpwardButton(index): ""}
        {index !== this.props.data.length-1 ?this.createDownwardButton(index): ""}
        
        {this.createEditButton(theLink)}


        <a data-toggle="tooltip" title="Delete Event" onClick={
          () => {     
            this.deleteAction(this.props.fromObjectInArray ? item.meet_id+"/sessions/"+item.session_id+"/events/"+item.event_id : index, theLink)
          }
        }>
          <span className="btn btn-danger btn-icon delete"><i className="material-icons">delete</i></span></a>


      </td>
    </tr>)
  }

  handleChange(event) {
    this.setState({ filter: event.target.value });
    if (event.target.value.length == 0) {
      //Reset
      this.setState({ data: this.props.data });
    } else {
      //Do the filtering
      //Go throught the fields
      var itemToShow = [];
      this.props.data.map((item, index) => {
        var stringRepresnetation = JSON.stringify(item);
        //if(stringRepresnetation.indexOf(event.target.value)!=-1){
        if (stringRepresnetation.toLowerCase().indexOf(event.target.value.toLowerCase()) != -1) {
          itemToShow.push(item);
        }
      })
      this.setState({ data: itemToShow });
    }

    console.log(event.target.value);
  }

  render() {
    return (
      <div>
        {/* <ConditionalDisplay
          condition={Config.adminConfig.showSearchInTables}
        >
          <div className="row">
            <div className="col-md-8"></div>
            <div className="col-md-4">
              <div className="form-group form-search is-empty">
                <input type="text" className="form-control" placeholder=" Search " value={this.state.filter} onChange={this.handleChange} />
                <span className="material-input"></span>
                <span className="material-input"></span>
              </div>
            </div>
          </div>
        </ConditionalDisplay> */}
        <div className="table-responsive">
          <table className="table datatable table-striped table-no-bordered table-hover">
            {/*JSON.stringify(this.props.data)*/}
            <thead>
              <tr>
                <th>Sequence</th>
                {this.state.headers ? this.state.headers.map((key) => {
                  return (<th>{Common.capitalizeFirstLetter(key)}</th>)
                }) : ""}
                <th className="disabled-sorting text-right">Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.props.data ? this.props.data.map((item, index) => {
                return this.createTableRow(item, index);
              }) : ""}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
export default SimpleTable;

SimpleTable.propTypes = {
  data: PropTypes.array.isRequired,
  headers: PropTypes.array.isRequired,
  routerPath: PropTypes.string.isRequired,
  isJustArray: PropTypes.bool.isRequired,
  sub: PropTypes.string,
  fromObjectInArray: PropTypes.bool.isRequired,
  deleteFieldAction: PropTypes.func.isRequired,
  swapSequence: PropTypes.func.isRequired
};
