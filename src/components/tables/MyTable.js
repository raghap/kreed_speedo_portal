import React, {Component,PropTypes} from 'react'
import Config from   '../../config/app';
import Common from '../../common.js'
import { Link, IndexLink, withRouter } from 'react-router'
import SweetAlert from 'react-bootstrap-sweetalert';

const ConditionalWrap = ({condition, wrap, children}) => condition ? wrap(children) : children;
const ConditionalDisplay = ({condition, children}) => condition ? children : <div></div>;

class MyTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      headers:this.props.headers,
      data:this.props.data,
      filter:"",
      showattachment:false
    };
    this.createTableRow=this.createTableRow.bind(this);
    this.deleteAction=this.deleteAction.bind(this);
    this.handleChange=this.handleChange.bind(this);
    this.downloadIdentityCard=this.downloadIdentityCard.bind(this);
    this.downloadPdf=this.downloadPdf.bind(this);
    this.moveAthleteButton=this.moveAthleteButton.bind(this);

      //Alerts related
      this.hideAlert=this.hideAlert.bind(this);
      this.showAlert=this.showAlert.bind(this);
    this.showAttachmentFileNames=this.showAttachmentFileNames.bind(this);
  }

  /**
  * componentDidMount event of React, fires when component is mounted and ready to display
  * Start connection to firebase
  */
  componentDidMount() {
    console.log(this.props);

    if(this.props.headers&&this.props.headers.length>0){
      //We already know who are our headers,preset
    }else{
      //Loop throught all items ( in data ) to find our headers
      var headers=[]
      var headersCounter={};
      for (var i = 0; i < this.props.data.length; i++) {

        //The type of our array items
        var parrentType=Common.getClass(this.props.data);
        var type=Common.getClass(this.props.data[i]);
        console.log("parrentType type is "+parrentType);
        console.log("Inside type is "+type);

        //OBJECTS INSIDE
        if(type=="Object"){

          //In CASE we have OBJECT as array items
          for (var key in this.props.data[i]) {
              // skip loop if the property is from prototype
              if (!this.props.data[i].hasOwnProperty(key)) continue;

              var obj = this.props.data[i][key];
              var objType = Common.getClass(obj);

              //Consider onyl String, Bool, Number
              if((objType==="String"||objType==="Boolean"||objType==="Number")&&
                  key!="uidOfFirebase"&&key!="clubID")
              {
                if(headersCounter[key]){
                  headersCounter[key]++
                }else{
                  headersCounter[key]=1;
                }
              }
          }
        }

        //STRING INSIDE
        else if(type=="String"){
          headers=["Value"];
          headersCounter["Value"]=1;
          break;
        }
      }

      console.log("headersCounter")
      console.log(headersCounter)
      //END looking for headers

     var numHeadersCounter=0;
     for (var key in headersCounter) {
       numHeadersCounter++;
     }

     console.log("numHeadersCounter "+numHeadersCounter);

     //ARRAYS INSIDE
     if(numHeadersCounter==0){
       console.log("Make it ArtificialArray");
       headers=["Items"];
       headersCounter["Items"]=1;
       type="ArtificialArray"; //Artificial
     }

      //Now we have the headers, with their number of occurences
      //Convert object to array
      var headersCounterAsArray=[];
      for (var key in headersCounter) {
        headersCounterAsArray.push({key:key,counter:headersCounter[key]})
      }


      headersCounterAsArray.sort(function(b, a) {
        return parseFloat(a.counter) - parseFloat(b.counter);
      });

      console.log("headersCounterAsArray length "+headersCounterAsArray.length)
      console.log(headersCounterAsArray)


      //Pick headers based on their number of appereances 2
      headers=[];
      for (var k = 0; k < headersCounterAsArray.length && k<Config.adminConfig.maxNumberOfTableHeaders; k++) {
        console.log("Is it ok "+(k < headersCounterAsArray.length && k<Config.adminConfig.maxNumberOfTableHeaders))
        headers.push(headersCounterAsArray[k].key)
      }

      //Update the state
      console.log(headers);
      this.setState({headers:headers,type:type})
    }


  }

  deleteAction(index,theLink){
    console.log(index);
    if(this.props.isFirestoreSubArray){
      this.props.deleteFieldAction(index,true,theLink);
    }else{
       this.props.deleteFieldAction(index,true);
    }
   
  }

  downloadIdentityCard(Kreed_ID, type){
    if(Kreed_ID){
      var thelink="https://us-central1-kreed-of-sports.cloudfunctions.net/genID?krid="+Kreed_ID+"&type="+type;
      return(  
        <a data-toggle="tooltip" title="Download Identity Card" href={thelink} target="_blank">
        <span className="btn btn-warning btn-icon margin-lr-5 delete"><i className="material-icons">credit_card</i></span></a>
      )
    }
}
  downloadPdf(Kreed_ID){
   
      if(Kreed_ID){
        var thelink="https://us-central1-kreed-of-sports.cloudfunctions.net/genForm?krid="+Kreed_ID;
        return(  
          <a data-toggle="tooltip" title="Download Registration Form" href={thelink} target="_blank">
          <span className="btn btn-info btn-icon margin-lr-5 delete"><i className="material-icons">file_download</i></span></a>
        )
      }
}

moveAthleteButton(Kreed_ID , theLink){
    return(  
      <a onClick={()=>{ this.props.moveAthleteAlert(Kreed_ID , theLink)}} data-toggle="tooltip" title="Move Athlete" >
      <span className="btn btn-primary btn-icon margin-lr-5 delete"><i className="material-icons">shuffle</i></span></a>
    )
}


showAlert(name){
  this.setState({[name]:true});
}
hideAlert(name) {
  // e.preventDefault(e);
  this.setState({[name]:false});
}

showSweetAlert(theLink){
  this.showAlert("showattachment");
}
//to display attachment files name
showAttachmentFileNames(){
  
}




  createEditButton(theLink){
      if(this.props.isFirestoreSubArray){
        return (<a onClick={()=>{ this.props.showSubItems(theLink)}}><span className="btn btn-success btn-icon margin-lr-5 edit"><i className="material-icons">mode_edit</i></span></a>);
      }else{
        return (<Link  data-toggle="tooltip" title="View Details"  to={theLink}>
          <span className="btn btn-success btn-icon margin-lr-5 edit"><i className="material-icons">remove_red_eye</i></span>
        </Link>)
    }
  }



  createTableRow(item,index){
    var theLink=this.props.routerPath.replace(":sub","")+this.props.sub;

    //Kreed_Id pre-processing
    var Kreed_Id= item["Kreed_ID"];
    var type="athletes";
    if(Kreed_Id) {
      var routeParts = theLink.split('+');
      type = routeParts.pop();
      if(type.startsWith("by"))
        type = routeParts.pop();
      if(!item["photo"] || !item["KSA_ID"] || item["photo"].indexOf("generic_") >= 0)
        Kreed_Id="";  
    }

    if(theLink.includes("{clubID}"))
    {
      var parts = theLink.replace(/\{(.+?)\}/g, item.clubID).split('+');
      var query = parts[parts.length - 1];
      if(query == "byClub" || query == "byGroup")
        parts.pop();
      theLink = parts.join('+');
    }

    if(this.props.isFirestoreSubArray){
      if(this.props.fromObjectInArray){
        theLink+=Config.adminConfig.urlSeparatorFirestoreSubArray+item.uidOfFirebase;
      }else{
          if(this.props.isJustArray){
              theLink+=Config.adminConfig.urlSeparatorFirestoreSubArray+index;
          }else{
              theLink+=Config.adminConfig.urlSeparatorFirestoreSubArray+this.props.name+Config.adminConfig.urlSeparatorFirestoreSubArray+index;
          }
      }
    }else{
      if(this.props.fromObjectInArray){
        theLink+=Config.adminConfig.urlSeparator+item.uidOfFirebase;
      }else{
          if(this.props.isJustArray){
              theLink+=Config.adminConfig.urlSeparator+index;
          }else{
              theLink+=Config.adminConfig.urlSeparator+this.props.name+Config.adminConfig.urlSeparator+index;
          }
      }
    }

  {/* added by PP to disable linking to edit pages 
    if(true)
      theLink="";*/}

    return (<tr>
        {this.state.headers?this.state.headers.map((key,subindex)=>{
          if(Config.adminConfig.fieldsTypes.photo.indexOf(key)>-1){
            //This is photo
            return (<td><div className="tableImageDiv" ><Link to={theLink}><img className="tableImage"  src={item[key]}  width={"200px"} /></Link></div></td>)
          }else{
            //Normal value
            //But can be string
            if(this.state.type=="String"){
                return subindex==0?(<td><Link to={theLink}>{item}</Link></td>):(<td>{item}</td>)
            }  if(this.state.type=="ArtificialArray"){
                  if(Config.adminConfig.showItemIDs){
                    return subindex==0?(<td><Link to={theLink}>{this.props.data[subindex].uidOfFirebase}</Link></td>):(<td>{this.props.data[subindex].uidOfFirebase}</td>)
                  }else{
                    return subindex==0?(<td><Link to={theLink}>{"Item "+(index+1)}</Link></td>):(<td>{"Item "+(index+1)}</td>)
                  }
                 
              }else{
                return subindex==0?(<td><Link to={theLink}>{item[key]}</Link></td>):(<td>{item[key]}</td>)
            }

          }

        }):""}

        {/* Commented out by PP temporarily since edits/deletes are disabled in the KSA portal */}
        <td className="td-actions text-right">
        {this.downloadIdentityCard(Kreed_Id, type)}
        {type == "athletes" ? this.downloadPdf(Kreed_Id) : ""}
        {type == "athletes" ? this.moveAthleteButton(Kreed_Id , theLink) : ""}
        
        {this.createEditButton(theLink)}
          {/*<a onClick={
            ()=>{ 
              this.deleteAction(this.props.fromObjectInArray?item.uidOfFirebase:index,theLink)
             }
           }>
           <span className="btn btn-danger btn-icon delete"><i className="material-icons">do_not_disturb_on</i></span></a> */}


        </td>

    </tr>)
  }

  handleChange(event) {
    this.setState({filter: event.target.value});
    if(event.target.value.length==0){
      //Reset
      this.setState({data: this.props.data});
    }else{
      //Do the filtering
       //Go throught the fields
      var itemToShow=[];
      this.props.data.map((item,index)=>{
        var stringRepresnetation=JSON.stringify(item);
        //if(stringRepresnetation.indexOf(event.target.value)!=-1){
        if (stringRepresnetation.toLowerCase().indexOf(event.target.value.toLowerCase()) != -1) {
          itemToShow.push(item);
        }
      })
      this.setState({data: itemToShow});
    }
   
    console.log(event.target.value);
  }

  render() {
    return (
      <div>
        <ConditionalDisplay
          condition={Config.adminConfig.showSearchInTables}
        >
          <div className="row">
            <div className="col-md-8"></div>
            <div className="col-md-4">
              <div className="form-group form-search is-empty">
                <input type="text" className="form-control" placeholder=" Search " value={this.state.filter} onChange={this.handleChange} />
                <span className="material-input"></span>
                <span className="material-input"></span>
              </div>
            </div>
          </div>
        </ConditionalDisplay>
          {/* sweet phone alert */}
            <SweetAlert
            show={this.state.showattachment} 
            onConfirm={() => {this.hideAlert("showattachment")}}>
            Attachments
          {this.showAttachmentFileNames()}
            </SweetAlert>

        <div className="table-responsive">  
        <table className="table datatable table-striped table-no-bordered table-hover">
        {/*JSON.stringify(this.props.data)*/}
            <thead>
                <tr>
                {this.state.headers?this.state.headers.map((key)=>{
                  return (<th>{Common.capitalizeFirstLetter(key)}</th>)
                }):""}
                    <th className="disabled-sorting text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                {this.state.data?this.state.data.map((item,index)=>{
                  return this.createTableRow(item,index);
                }):""}
            </tbody>
        </table>
        </div>
      </div>
    )
  }
}
export default MyTable;

//http://localhost:3000/#/athlete/aquaticsID+India+associations+KSA+clubs+{clubID}+athletes+byClub+phJcCHwPDUkPYLN6gzC6

MyTable.propTypes = {
    data:PropTypes.array.isRequired,
    headers: PropTypes.array.isRequired,
    routerPath: PropTypes.string.isRequired,
    isJustArray: PropTypes.bool.isRequired,
    sub:PropTypes.string,
    fromObjectInArray:PropTypes.bool.isRequired,
    deleteFieldAction:PropTypes.func.isRequired,
    moveAthleteAlert:PropTypes.func
};
